## Summary

This Dumux module contains the code to generate the results of the paper below:

Vahabzadeh Asbaghi E., Buntic I., Nazari F., Flemisch B., Helmig R., Niasar V.

Application of Vertical Equilibrium in Underground Hydrogen Storage Simulation
[Link to Paper](https://doi.org/10.1016/j.ijhydene.2025.01.201)
## Installation


In order to install this module and all other necessary dune-modules, please execute the following steps:

```bash
mkdir your_target_folder_name
cd your_target_folder_name
wget https://git.iws.uni-stuttgart.de/dumux-pub/vahabzadeh2024a/-/raw/main/install_vahabzadeh2024a.py
python3 install_vahabzadeh2024a.py
```

## Tests

- To execute the test for the 2D method, head to `DUMUX/vahabzadeh2024a/build-cmake/test/porousmediumflow/2p/` and run
```bash
make test_2p_incompressible_tpfa_plume
./test_2p_incompressible_tpfa_plume 
```
The results can then be inspected via
```bash
paraview test_cc2p_plume_.pvd
```
- To execute the test for the VE method, head to `DUMUX/vahabzadeh2024a/build-cmake/test/porousmediumflow/2pve/` and run
```bash
make test_2pve_incompressible_tpfa_plume
./test_2pve_incompressible_tpfa_plume
```
The results can then be inspected via
```bash
paraview VE.pvd VE_fine.pvd
```

## Versions

 |              module name              |      branch name      |                 commit sha                 |         commit date         |
 |---------------------------------------|-----------------------|--------------------------------------------|-----------------------------|
 |              dune-subgrid             |     origin/master     |  41ab447c59ea508c4b965be935b81928e7985a6b  |  2022-09-25 23:18:45 +0000  |
 |            vahabzadeh2024a            |      origin/main      |                      -                     |  2023-11-23 11:09:15 +0100  |
 |          dune-localfunctions          |  origin/releases/2.9  |  fc5c8050452c59c335e6be28afb761bdbd4479ad  |  2023-08-21 22:09:53 +0000  |
 |             dune-geometry             |  origin/releases/2.9  |  1051f4d4e7ea10ec7787e49974c4ff5d4debc176  |  2023-07-15 07:21:03 +0000  |
 |              dune-common              |  origin/releases/2.9  |  b600cb2928aec15eb575fa50cbf8df4042fc33c4  |  2023-11-21 08:11:22 +0000  |
 |                 dumux                 |  origin/releases/3.7  |  8eaf65d6b3ce79fe2b948eb248039122147ee3bf  |  2023-11-13 08:08:12 +0000  |
 |              dune-uggrid              |  origin/releases/2.9  |  4a601143bd7aa3617eab6f7391230976e06ff43b  |  2022-11-25 22:10:57 +0000  |
 |               dune-grid               |  origin/releases/2.9  |  40883adafea16d2a9c528fdf11f0ad8bb08a2f97  |  2023-11-04 14:39:39 +0000  |
 |               dune-istl               |  origin/releases/2.9  |  1582b9e200ad098d0f00de2c135f9eed38508319  |  2023-10-19 09:15:16 +0000  |

## License

This project is licensed under the terms and conditions of the GNU General Public
License (GPL) version 3 or - at your option - any later version.
The GPL can be found under [GPL-3.0-or-later.txt](LICENSES/GPL-3.0-or-later.txt)
provided in the `LICENSES` directory located at the topmost of the source code tree.
