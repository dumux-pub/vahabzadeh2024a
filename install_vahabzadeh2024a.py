#!/usr/bin/env python3

# 
# This installs the module vahabzadeh2024a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |              module name              |      branch name      |                 commit sha                 |         commit date         |
# |---------------------------------------|-----------------------|--------------------------------------------|-----------------------------|
# |              dune-subgrid             |     origin/master     |  41ab447c59ea508c4b965be935b81928e7985a6b  |  2022-09-25 23:18:45 +0000  |
# |            vahabzadeh2024a            |      origin/main      |                      -                     |  2023-11-23 11:09:15 +0100  |
# |          dune-localfunctions          |  origin/releases/2.9  |  fc5c8050452c59c335e6be28afb761bdbd4479ad  |  2023-08-21 22:09:53 +0000  |
# |             dune-geometry             |  origin/releases/2.9  |  1051f4d4e7ea10ec7787e49974c4ff5d4debc176  |  2023-07-15 07:21:03 +0000  |
# |              dune-common              |  origin/releases/2.9  |  b600cb2928aec15eb575fa50cbf8df4042fc33c4  |  2023-11-21 08:11:22 +0000  |
# |                 dumux                 |  origin/releases/3.7  |  8eaf65d6b3ce79fe2b948eb248039122147ee3bf  |  2023-11-13 08:08:12 +0000  |
# |              dune-uggrid              |  origin/releases/2.9  |  4a601143bd7aa3617eab6f7391230976e06ff43b  |  2022-11-25 22:10:57 +0000  |
# |               dune-grid               |  origin/releases/2.9  |  40883adafea16d2a9c528fdf11f0ad8bb08a2f97  |  2023-11-04 14:39:39 +0000  |
# |               dune-istl               |  origin/releases/2.9  |  1582b9e200ad098d0f00de2c135f9eed38508319  |  2023-10-19 09:15:16 +0000  |

import os
import sys
import subprocess

top = "DUMUX"
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.split("/")[-1]
    if targetFolder.endswith(".git"):
        targetFolder = targetFolder[:-4]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid.git", "origin/master", "41ab447c59ea508c4b965be935b81928e7985a6b", )

print("Installing vahabzadeh2024a")
installModule("vahabzadeh2024a", "https://git.iws.uni-stuttgart.de/dumux-pub/vahabzadeh2024a.git", "origin/main", "origin/main", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.9", "fc5c8050452c59c335e6be28afb761bdbd4479ad", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.9", "1051f4d4e7ea10ec7787e49974c4ff5d4debc176", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.9", "b600cb2928aec15eb575fa50cbf8df4042fc33c4", )

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/releases/3.7", "8eaf65d6b3ce79fe2b948eb248039122147ee3bf", )

print("Installing dune-uggrid")
installModule("dune-uggrid", "https://gitlab.dune-project.org/staging/dune-uggrid", "origin/releases/2.9", "4a601143bd7aa3617eab6f7391230976e06ff43b", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.9", "40883adafea16d2a9c528fdf11f0ad8bb08a2f97", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.9", "1582b9e200ad098d0f00de2c135f9eed38508319", )

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/cmake.opts', 'all'],
    '.'
)
