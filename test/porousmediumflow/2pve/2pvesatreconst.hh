// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_TEST_TWOPVE_2PVESATRECONST_HH
#define DUMUX_TEST_TWOPVE_2PVESATRECONST_HH

#include "properties.hh"

#include <filesystem>

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPVESatReconst;

namespace Properties {

} // end namespace Properties

/*!
 * \ingroup TwoPTests
 * \brief The incompressible 2p test problem.
 */
template<class TypeTag>
class TwoPVESatReconst
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    using CellArray = std::array<unsigned int, dimWorld>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;


public:
    TwoPVESatReconst(Problem& problem, SolutionVector& curSol) : problem_(problem), curSol_(curSol), 
                                                                 pe_(getParam<Scalar>("SpatialParams.BrooksCoreyPcEntry")), lambda_(getParam<Scalar>("SpatialParams.BrooksCoreyLambda"))
    {
        int columnIndex = 0;
        for (const auto& element : elements(problem_.gridGeometry().gridView()))
        {
            // identify column number
            GlobalPosition globalPos = element.geometry().center();
            CellArray numberOfCellsX = getParam<CellArray>("Grid.Cells");
            double deltaX = problem_.gridGeometry().bBoxMax()[0]/numberOfCellsX[0];

            columnIndex = round((globalPos[0] + (deltaX/2.0))/deltaX);

            mapColumns_.insert(std::make_pair(columnIndex, element));
            dummy_ = element; //TODO: Is this good? //TODO: dummy_ is only needed to define volVars in calculatePlumeDist
        }
        
        deleteFilesWithExtensionDotOut();
    }

    void deleteFilesWithExtensionDotOut()
    {
        std::filesystem::path path = std::filesystem::current_path();
        for(const auto & file : std::filesystem::directory_iterator(path))
        {
            std::string path_string{file.path().u8string()};
            if(path_string.substr(path_string.length() - 4 ) == ".out")
            {
                remove(file.path());
            }
        }
    }
    
    void update(Problem& problem, const SolutionVector& curSol)
    {
        problem_ = problem;
        curSol_ = curSol;
    }


    Scalar referencePressureAtPos(const GlobalPosition& globalPos)
    {
        return 1.0e7;
    }


    Scalar referenceTemperatureAtPos(const GlobalPosition& globalPos)
    {
        return 326.0;
    }


    Scalar calculateGasPlumeDist(const Element& element, Scalar satW)
    {
        const auto someElemSol = elementSolution(element, curSol_, problem_.gridGeometry());
        auto someFvGeometry = localView(problem_.gridGeometry());
        someFvGeometry.bindElement(element);
        const auto someScv = *(scvs(someFvGeometry).begin());
        VolumeVariables volVars;
        volVars.update(someElemSol, problem_, element, someScv);

        const auto fluidMatrixInteraction = problem_.spatialParams().fluidMatrixInteractionAtPos(dummy_.geometry().center());
        
        Scalar swr = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
        Scalar snr = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
    
        GlobalPosition globalPos = element.geometry().center();
        Scalar domainHeight = problem_.gridGeometry().bBoxMax()[dim-1];
        Scalar resSatW = swr;
        Scalar resSatN = snr;
        Scalar gravity = -problem_.spatialParams().gravity(globalPos)[1];

        Scalar gasPlumeDist = 0.0;

        Scalar densityW = volVars.density(waterPhaseIdx);
        Scalar densityNw = volVars.density(gasPhaseIdx);
        Scalar lambda = lambda_;
        Scalar entryP = pe_;

        //check if GasPlumeDist>0, GasPlumeDist=0, GasPlumeDist<0
        Scalar fullIntegral =
        1.0 / (1.0 - lambda) * std::pow((domainHeight * (densityW - densityNw) * gravity + entryP),(1.0 - lambda))
        * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityNw) * gravity)
        + resSatW * domainHeight
        - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityNw) * gravity);
        Scalar residual = 1.0;

        //gasPlumeDist > 0.0
        if(fullIntegral < satW*domainHeight)
        {
            gasPlumeDist = domainHeight / 2.0; //XiStart
            //solve equation for
            int count = 0;
            for (; count < 100; count++)
            {
                residual =
                1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP),(1.0 - lambda))
                * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityNw) * gravity)
                + resSatW * (domainHeight - gasPlumeDist)
                - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityNw) * gravity)
                + gasPlumeDist
                - satW * domainHeight;

                if (fabs(residual) < 1e-12)
                {
                    break;
                }

                Scalar derivation =
                std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP), -lambda)
                * (- 1.0 + resSatN + resSatW) * std::pow(entryP, lambda) - resSatW + 1.0;

                gasPlumeDist = gasPlumeDist - residual / (derivation);
            }
        }
        //GasPlumeDist<0
        else if (fullIntegral > satW * domainHeight)
        {
            gasPlumeDist = 0.0; //XiStart
            //solve equation
            int count = 0;
            for (; count < 100; count++)
            {
                residual =
                1.0 / (1.0 - lambda)
                * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP), (1.0 - lambda))
                * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityNw) * gravity)
                + resSatW * domainHeight
                - 1.0 / (1.0 - lambda) * std::pow((entryP - gasPlumeDist * (densityW - densityNw)
                * gravity),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda)
                / ((densityW - densityNw) * gravity)
                - satW * domainHeight;

                if (fabs(residual) < 1e-10)
                {
                    break;
                }

                Scalar derivation =
                std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP), -lambda)
                * (- 1.0 + resSatN + resSatW) * std::pow(entryP, lambda)
                + std::pow((entryP - gasPlumeDist * (densityW - densityNw) * gravity), -lambda)
                * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda);

                gasPlumeDist = gasPlumeDist - residual / (derivation);
            }
        }
        //GasPlumeDist=0
        else
        {
            gasPlumeDist = 0.0;
        }

        return gasPlumeDist;
    }


    /*! \brief Calculates reconstructed saturation for VE column, dependent on z
     *
     */
    Scalar reconstSaturation(Scalar height, Scalar gasPlumeDist)
    {
        GlobalPosition globalPos = dummy_.geometry().center();

        const auto someElemSol = elementSolution(dummy_, curSol_, problem_.gridGeometry());
        auto someFvGeometry = localView(problem_.gridGeometry());
        someFvGeometry.bindElement(dummy_);
        const auto someScv = *(scvs(someFvGeometry).begin());
        VolumeVariables volVars;
        volVars.update(someElemSol, problem_, dummy_, someScv);

        const auto fluidMatrixInteraction = problem_.spatialParams().fluidMatrixInteractionAtPos(dummy_.geometry().center());
        
        Scalar swr = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
        Scalar snr = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();

        Scalar resSatW = swr;
        Scalar resSatN = snr;
        Scalar densityW = volVars.density(waterPhaseIdx);
        Scalar densityNw = volVars.density(gasPhaseIdx);
        Scalar entryP = pe_;
        Scalar lambda = lambda_;

        Scalar reconstSaturation = 0.0;

        reconstSaturation = 1.0;
        if(height > gasPlumeDist)
        {
            reconstSaturation = std::pow(((height - gasPlumeDist) * (densityW - densityNw) * (problem_.spatialParams().gravity(globalPos)).two_norm() + entryP), (-lambda))
            * std::pow(entryP, lambda) * (1.0 - resSatW - resSatN) + resSatW;
        }

        return reconstSaturation;
    }


    /*! \brief Calculates integral of difference of wetting saturation over z
     *
     */
    Scalar calculateSatCriterionIntegral(Scalar lowerBound, Scalar upperBound, Scalar satW, Scalar gasPlumeDist)
    {
        int intervalNumber = 10;
        Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

        Scalar satIntegral = 0.0;
        for(int count=0; count<intervalNumber; count++ )
        {
            satIntegral += std::abs((reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist)
                    + reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist))/2.0 - satW);
        }
        satIntegral = satIntegral * deltaZ;

        return satIntegral;
    }


    void printVECriterionOverDomain(const int timeStepIdx)
    {
        std::cout << "timeStepIdx: " << timeStepIdx << std::endl;

        const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
        for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
        {
            //iterate over all cells in one grid column and calculate averageSatColumn
            Scalar averageSatColumn = 0;
            Scalar volumeColumn = 0;
            typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
            for (; it != mapColumns_.upper_bound(columnIndex); ++it)
            {
                const Scalar volume = (it->second).geometry().volume();
                unsigned int globalIdxI = problem_.gridGeometry().elementMapper().index(it->second);
                const Scalar saturationW = 1.0 - curSol_[globalIdxI][1];
                
                averageSatColumn += saturationW * volume;
                volumeColumn += volume;
            }
            averageSatColumn /= volumeColumn;


            Scalar gasPlumeDist = calculateGasPlumeDist(dummy_, averageSatColumn);

            //iterate over all cells in one grid column and calculate VE criteria
            Scalar positionX;
            Scalar satCrit = 0;
            static const Scalar domainHeight = problem_.gridGeometry().bBoxMax()[dim - 1];
            static const Scalar deltaZ = domainHeight/numberOfCells[dim-1];
            it = mapColumns_.lower_bound(columnIndex);
            for (; it != mapColumns_.upper_bound(columnIndex); ++it)
            {
                const GlobalPosition globalPos = (it->second).geometry().center();
                positionX = globalPos[0];
                const Scalar lowerBound = globalPos[dim-1] - deltaZ/2.0;
                const Scalar upperBound = globalPos[dim-1] + deltaZ/2.0;
                const int globalIdxI = problem_.gridGeometry().elementMapper().index(it->second);
                const Scalar saturationW = 1.0 - curSol_[globalIdxI][1];
                satCrit += calculateSatCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist);
            }

            satCrit /= (domainHeight-gasPlumeDist);

            if(averageSatColumn > 1.0-eps_)
            {
                satCrit = 0;
            }

            std::ostringstream oss;
            oss << "satCrit_" << timeStepIdx << ".out";
            std::string fileName = oss.str();
            outputFile_.open(fileName, std::ios::app);
            outputFile_ << positionX << " " << satCrit << std::endl;
            outputFile_.close();
        }
    }

protected:
    Problem& problem_;
    SolutionVector& curSol_;

private:
    std::multimap<int, Element> mapColumns_;
    static constexpr Scalar eps_ = 1e-6;
    Element dummy_;
    std::ofstream outputFile_;
 
    Scalar pe_;
    Scalar lambda_;
};

} // end namespace Dumux

#endif
