// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_TEST_TWOPVE_PROBLEM_HH
#define DUMUX_TEST_TWOPVE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/io/container.hh>
#include <dumux/common/fvproblem.hh>

#include <dumux/verticalequilibriumcommon/quantitystorage.hh>

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

namespace Dumux {

/*!
 * \ingroup TwoPTests
 * \brief The incompressible 2p test problem.
 */
template<class TypeTag>
class TwoPVETestProblem : public FVProblem<TypeTag>
{
    //Here, the spatialparams take a parameter "paramGroup" which is not handled by the spatialParams constructor used in
    //FVProblemWithSpatialParams. Therefore init spatialParams here and instead of inheriting from FVProblemWithSpatialParams, inherit from FVProblem directly.
    using ParentType = FVProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        pressureH2OIdx = Indices::pressureIdx,
        saturationGasIdx = Indices::saturationIdx,
        contiGasEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using QuantityReconstructor = TwoPVEQuantityReconst<TypeTag>;

    using CellArray = std::array<unsigned int, dimWorld>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

    using QuantityStorage = TwoPVEQuantityStorage<TypeTag>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using WettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::WettingPhase;
    using NonwettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::NonwettingPhase;

public:
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    //constructor for the fine-scale problem
    TwoPVETestProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                      std::shared_ptr<const Problem> problemCoarse)
        : ParentType(gridGeometry, problemCoarse->paramGroup()),
          isCoarseVEModel_(false),
          numberOfCells_(problemCoarse->getNumberOfCells()),
          upperRightDomain_(problemCoarse->getUpperRightDomain()),
          lowerLeftDomain_(problemCoarse->getLowerLeftDomain()),
          deltaX_(problemCoarse->getDeltaX()),
          deltaZ_(problemCoarse->getDeltaZ()),
          quantityReconstructor_(problemCoarse->getQuantityReconstructor()),
          quantityStorage_(problemCoarse->getQuantityStorage()),
          spatialParams_(std::make_shared<SpatialParams>(gridGeometry, deltaZ_, isCoarseVEModel_))
    {
        problemName_  =  getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name") + "_fine";
        DIMS_s=getParam<GlobalPosition>("Grid.Cells");
    }

    void setTimeStep(double newTimeStep) { time_step = newTimeStep; } // Public setter method for time_step

    void setTimeStepSize(double new_time_step_size) { time_step_size = new_time_step_size; } // Public setter method for time_step

    //constructor for the coarse-scale problem which also takes the gridGeometry of the fine grid
    TwoPVETestProblem(std::shared_ptr<const GridGeometry> gridGeometryCoarse,
                      std::shared_ptr<const GridGeometry> gridGeometryFine,
                      std::shared_ptr<QuantityReconstructor> quantityReconstructor,
                      const std::string& modelParamGroup = "")
        : ParentType(gridGeometryCoarse, modelParamGroup),
          problemName_(getParamFromGroup<std::string>(modelParamGroup, "Problem.Name")),
          isCoarseVEModel_(true),
          numberOfCells_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.FineCells")),
          upperRightDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.UpperRight")),
          lowerLeftDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.LowerLeft")),
          deltaX_(((double)upperRightDomain_[0] - (double)lowerLeftDomain_[0])/(double)numberOfCells_[0]),
          deltaZ_(((double)upperRightDomain_[1] - (double)lowerLeftDomain_[1])/(double)numberOfCells_[1]),
          quantityReconstructor_(quantityReconstructor),
          spatialParams_(std::make_shared<SpatialParams>(gridGeometryCoarse, deltaZ_, isCoarseVEModel_, problemName_))

    {
        DIMS_s=getParam<GlobalPosition>("Grid.Cells");
        std::cout << "in problem: problemName_: " << problemName_ << std::endl;
        quantityStorage_ = std::make_shared<QuantityStorage>(upperRightDomain_, gridGeometryCoarse, gridGeometryFine);
        //create and store a multimap which includes the columnIdx as the key and maps it to all the fine-scale elements belonging to this column as a shared_ptr
        mapColumns_ = std::make_shared<std::multimap<int, Element>>(VerticalEquilibriumModelUtil::createColumnMultiMapNew<Element, GridGeometry, Scalar>(gridGeometryFine, gridGeometryCoarse));
        setSpatialParamsMapColumns_(mapColumns_);
        calcPermeabilityAndPorosityCoarseInSpatialParams_();
    }

    void setH2rate(Scalar H2rate)
    { H2rate_ = H2rate; }

    /*!
     * \brief Gets the time counter.
     */
     Scalar getH2rate() const
    {
        return H2rate_;
    }

    void setH2Orate(Scalar H2Orate)
    { H2Orate_ = H2Orate; }

    /*!
     * \brief Gets the time counter.
     */
    Scalar getH2Orate() const
    {
        return H2Orate_;
    }

    void updateH2rate(const GlobalPosition &globalPos,
                      const int coarseElementIdx)
    {
        NumEqVector values_check(0.0);

        auto upperBoundIterator = (*mapColumns_).upper_bound(coarseElementIdx);
        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(coarseElementIdx);
        VolumeVariables volVarsFine;

        if(globalPos[0]-deltaX_/2<eps_)
        {
            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {
                GlobalPosition globalPosFineElement = (it->second).geometry().center();
                volVarsFine.updateFine(*otherProblem_, it->second);
                globalPosFineElement[0] = globalPos[0] - 0.5*deltaX_; //TODO: this only holds for the left boundary
                std::pair<NumEqVector, double> result = neumannFineAtPos(globalPosFineElement, volVarsFine); //new implementation
                values_check += result.first*deltaZ_;
           }
            Scalar gas_rate = values_check[contiGasEqIdx];

            H2rate_ = gas_rate;
        }
    }

    void updateH2Orate(const GlobalPosition &globalPos,
                      const int coarseElementIdx)
    {
        NumEqVector values_check(0.0);
        auto upperBoundIterator = (*mapColumns_).upper_bound(coarseElementIdx);
        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(coarseElementIdx);
        VolumeVariables volVarsFine;

        if(globalPos[0]-deltaX_/2<eps_)
        {
            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {
                GlobalPosition globalPosFineElement = (it->second).geometry().center();
                volVarsFine.updateFine(*otherProblem_, it->second);
                globalPosFineElement[0] = globalPos[0] - 0.5*deltaX_; //TODO: this only holds for the left boundary

                std::pair<NumEqVector, double> result = neumannFineAtPos(globalPosFineElement, volVarsFine); //new implementation
                values_check += result.first*deltaZ_;
           }
            Scalar water_rate = values_check[contiWEqIdx];

            H2Orate_ = water_rate;
        }
    }


    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    const bool& isCoarseVEModel() const
    {
        return isCoarseVEModel_;
    }

//----------------------------------------------------------initial and boundary conditions start here----------------------------------------------------------

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        if (onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

//-----------------------------------DIRICHLET CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary sub control volume face
     */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);

        values = dirichletCoarse(element, scvf);

        return values;
    }


    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment of the coarse-scale grid. Intended for the coarse-scale grid.
     *
     * \param coarseElement The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary sub control volume face
     */
    PrimaryVariables dirichletCoarse(const Element &coarseElement, const SubControlVolumeFace &scvfCoarse) const
    {
        PrimaryVariables values(0.0);

        const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0

        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
        auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);

        //evaluate pressure at bottom of column
        GlobalPosition globalPosFineElementBottom = (it->second).geometry().center();
        globalPosFineElementBottom[1] -= 0.5*deltaZ_; //shift height by 0.5*cellHeight downwards, to evaluate p at the bottom of column and not in fineCell center
        values[pressureH2OIdx] = dirichletFineAtPos(globalPosFineElementBottom)[pressureH2OIdx]; //take pressure at bottom of domain as coarse-scale pressure

        for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
        {
            GlobalPosition globalPosFineElement = (it->second).geometry().center();
            globalPosFineElement[0] = scvfCoarse.center()[0];

            values[saturationGasIdx] += (dirichletFineAtPos(globalPosFineElement)[saturationGasIdx] * this->spatialParams().porosityFineAtElement(it->second))*deltaZ_; //integration of the saturation, see modelDescription
        }

        values[saturationGasIdx] /= this->spatialParams().porosityCoarseAtElement(coarseElement); //average saturation by coarse-scale porosity

        return values;
    }


    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment on the fine-scale grid. Intended for the fine-scale grid. Dirichlet conditions only need to be changed here.
     *
     * \param globalPos The global position of an element belonging to the fine scale.
     */
    PrimaryVariables dirichletFineAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;

        Scalar temperature = 326.0;
        Scalar pressureWInit = 1.0e7;

        Scalar densityW = this->returnDensityLiquidAndViscosity(temperature, pressureWInit)[0];

        bool enableGravity = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableGravity");

        if(enableGravity==true)
        {
            values[pressureH2OIdx] = pressureWInit - densityW * this->spatialParams().gravity(globalPos).two_norm() * globalPos[1];
        }
        else
        {
            values[pressureH2OIdx] = pressureWInit;
        }

        values[saturationGasIdx] = 0.0;

        return values;
    }

//-----------------------------------NEUMANN CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        values = neumannCoarse(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);

        return values;
    }


     /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment of the coarse-scale grid. Intended for the fine-scale grid.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     */
        NumEqVector neumannCoarse(const Element& coarseElement,
                              const FVElementGeometry& fvGeometry,
                              const ElementVolumeVariables& elemVolVars,
                              const ElementFluxVariablesCache& elemFluxVarsCache,
                              const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        NumEqVector values_check(0.0);
        GlobalPosition globalPosCoarseScvf = scvf.center();
        const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0
        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
        auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);
        auto time_const=timeLoop_; double Prod_sat=0.0;
        VolumeVariables volVarsFine;       
        if(onLeftBoundary_(globalPosCoarseScvf))
        {
            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {
                GlobalPosition globalPosFineElement = (it->second).geometry().center();
                volVarsFine.updateFine(*otherProblem_, it->second);
                globalPosFineElement[0] = scvf.center()[0];
                std::pair<NumEqVector, double> result = neumannFineAtPos(globalPosFineElement, volVarsFine); //new implementation
                values_check += result.first*deltaZ_;
                Prod_sat += result.second;
            }
                values[contiWEqIdx]=values_check[contiWEqIdx];
                values[contiGasEqIdx]=values_check[contiGasEqIdx];  
        }
      
        return values;
    }



    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment on the fine-scale grid. Intended for the fine-scale grid. Neumann conditions only need to be changed here.
     *
     * \param globalPos The position of the integration point of the boundary segment belonging to the fine-scale grid.
     */
    std::pair<NumEqVector, double> neumannFineAtPos(const GlobalPosition& globalPos,
                                 const VolumeVariables& fineVolVars) const
    {
        double Delta_z=0.0; double Delta_x=0.0; double prod_sat=0.0;
        Delta_z=(this->gridGeometry().bBoxMax()[1]-this->gridGeometry().bBoxMin()[1]);
        Delta_z=Delta_z/DIMS_s[1];
        Delta_x=(this->gridGeometry().bBoxMax()[0]-this->gridGeometry().bBoxMin()[0]);
        Delta_x=Delta_x/DIMS_s[0];
        auto time_const=timeLoop_;
        Scalar time_month=time_const->time()/2629800;
        Scalar inj_time = 0.0;
        Scalar INJ_DEPTH =0.0;
        inj_time= getParam<double>("BoundaryConditions.InjectionDurationOp"); // kg / (m * s)
        INJ_DEPTH= getParam<double>("BoundaryConditions.InjectionDepth"); // kg / (m * s)
        NumEqVector values(0.0);
        if ((globalPos[0]<eps_  && globalPos[1]>INJ_DEPTH && time_month<=inj_time) )
        {
            values[contiGasEqIdx] = getParam<double>("BoundaryConditions.InjectionRate"); // kg / (m * s)
            prod_sat=prod_sat + fineVolVars.saturation(1);
        }
        if ((globalPos[0]<eps_  && globalPos[1]>INJ_DEPTH && time_month>inj_time))
        {
            const Scalar elemMobW = fineVolVars.mobility(0);      //Mobilities
            const Scalar elemMobN = fineVolVars.mobility(1);
            prod_sat=prod_sat + fineVolVars.saturation(1);
            Scalar fw=elemMobW/(elemMobW+elemMobN);
            Scalar rate = getParam<double>("BoundaryConditions.Productionrate"); // kg / (m * s)
            const Scalar qN=(1-fw)*rate; 
            const Scalar qW=fw*rate;
            values[contiGasEqIdx] = qN;
            values[contiWEqIdx] = qW;
        }

        return std::make_pair(values, prod_sat);
    }

    void postTimeStep(const std::shared_ptr<SolutionVector> x,
                      std::shared_ptr< GridGeometry> fvGridGeometry,
                      TimeLoopPtr timeLoop)
    {
        Scalar sat=0.0; Scalar avg_sat_full = 0.0;
        // Scalar month=timeLoop_->time()/2629800;
        Scalar month=this->time_step/2629800;
        Scalar inj_rate=0.0;Scalar gas_rate=0.0;Scalar production_water_rate=0.0;
        VolumeVariables volVarsFine; Scalar qq=0; Scalar mm =0; Scalar inj_time=0.0;
        Scalar INJ_DEPTH =0.0;
        inj_time= getParam<double>("BoundaryConditions.InjectionDurationOp"); // kg / (m * s)
        INJ_DEPTH= getParam<double>("BoundaryConditions.InjectionDepth"); // kg / (m * s)
        for (const auto& element : Dune::elements(fvGridGeometry->gridView()))
        {
            //index of the element
            const int columnIdx = this->gridGeometry().elementMapper().index(element); //needs to start at 0
            typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
            auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);
            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {

                GlobalPosition globalPosFineElement = (it->second).geometry().center();
                volVarsFine.updateFine(*otherProblem_, it->second);
                avg_sat_full = avg_sat_full + volVarsFine.saturation(1);
                mm =mm+1;
                globalPosFineElement[0] = globalPosFineElement[0]-(deltaX_/2);
                if (globalPosFineElement[0]< eps_ && globalPosFineElement[1]> INJ_DEPTH){
                    qq=qq+1;
                    if (month <= inj_time){
                        inj_rate= getParam<double>("BoundaryConditions.InjectionRate")*deltaZ_; // kg / (m * s)
                        gas_rate = gas_rate + inj_rate;
                        sat=sat + volVarsFine.saturation(1);
                    }else if (month > inj_time){
                        sat=sat + volVarsFine.saturation(1);
                        const Scalar elemMobW = volVarsFine.mobility(0);      //Mobilities
                        const Scalar elemMobN = volVarsFine.mobility(1);
                        Scalar fw=elemMobW/(elemMobW+elemMobN);
                        Scalar rate = getParam<double>("BoundaryConditions.Productionrate")*deltaZ_; // kg / (m * s)
                        gas_rate=gas_rate+(1-fw)*rate;
                        production_water_rate=production_water_rate+fw*rate;
                    }
                }
            }
        }
        Scalar avg_prod_sat = sat/qq;
        avg_sat_full = avg_sat_full/mm;
        H2_VEC.push_back(gas_rate);
        prodsat_VEC.push_back(avg_prod_sat);
        sat_VEC.push_back(avg_sat_full);
        H2O_VEC.push_back(production_water_rate);
        time_VEC.push_back(this->time_step);
        writeContainerToFile(time_VEC, "time-VE.txt");
        writeContainerToFile(H2O_VEC, "H2O-VE.txt");
        writeContainerToFile(H2_VEC, "H2-VE.txt");
        writeContainerToFile(prodsat_VEC, "prodsat-VE.txt");
        writeContainerToFile(sat_VEC, "sat-VE.txt");
    }
//-----------------------------------INITIAL CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param element The element for which the initial condition is set
     */
    PrimaryVariables initial(const Element &element) const
    {
        PrimaryVariables values(0.0);
        GlobalPosition globalPos = element.geometry().center();

        if(this->isCoarseVEModel() == true)
        {
            values = initialCoarse(element);
        }
        else
        {
            values = initialFineAtPos(globalPos);
        }

        return values;
    }


        /*!
     * \brief Evaluates the initial values for a control volume of the coarse-scale grid. Intended for the fine-scale grid.
     *
     * \param coarseElement The coarse-scale element
     */
    PrimaryVariables initialCoarse(const Element& coarseElement) const
    {
        PrimaryVariables values(0.0);

        const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0
        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
        auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);

        GlobalPosition columnBottomCoordinate = coarseElement.geometry().center(); //create a coordinate at the bottom of the column for the pressure calculation
        columnBottomCoordinate[1] = lowerLeftDomain_[1]; //coordinate at the bottom of the column

        values[pressureH2OIdx] = initialFineAtPos(columnBottomCoordinate)[pressureH2OIdx]; //coarse-scale pressure is supposed to be the fine-scale pressure at the bottom of the column

        for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
        {
            GlobalPosition globalPosFineElement = (it->second).geometry().center();
            values[saturationGasIdx] += (initialFineAtPos(globalPosFineElement)[saturationGasIdx] * this->spatialParams().porosityFineAtElement(it->second))*deltaZ_;
        }

        values[saturationGasIdx] /= this->spatialParams().porosityCoarseAtElement(coarseElement);

        return values;
    }


    /*!
     * \brief Evaluates the initial values for a control volume of the fine-scale grid. Intended for the fine-scale grid. Initial conditions only need to be changed here.
     *
     * \param globalPos The global position of the scv belonging to the fine-scale grid.
     */
    PrimaryVariables initialFineAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);

        Scalar temperature = 326.0;
        Scalar pressureWInit = 1.0e7;

        Scalar densityW = this->returnDensityLiquidAndViscosity(temperature, pressureWInit)[0];

        bool enableGravity = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableGravity");

        if(enableGravity==true)
        {
            values[pressureH2OIdx] = pressureWInit - densityW * this->spatialParams().gravity(globalPos).two_norm() * globalPos[1];
        }
        else
        {
            values[pressureH2OIdx] = pressureWInit;
        }
        values[saturationGasIdx] = 0.0;

        return values;
    }


//----------------------------------------------------------initial and boundary conditions end here----------------------------------------------------------


    /*!
     * \brief Return the shared pointer to the quantityReconstructor given during initialization of problem
     */
    const std::shared_ptr<QuantityReconstructor> getQuantityReconstructor() const
    {
        return quantityReconstructor_;
    }


    /*!
     * \brief Return the number of cells of the fine-scale domain
     */
    const CellArray getNumberOfCells() const
    {
        return numberOfCells_;
    }


    /*!
     * \brief Return the number of cells in x direction which is also the number of elements in the coarse grid
     */
    int getNumberOfCellsX() const
    {
        return numberOfCells_[0];
    }

    /*!
     * \brief Return the number of cells in x direction which is also the number of elements in the coarse grid
     */
    int getNumberOfCellsY() const
    {
        return numberOfCells_[1];
    }

    /*!
     * \brief Return a shared pointer to the map containing the coarse element and their respective fine-scale elements
     */
    const std::shared_ptr<std::multimap<int, Element>> getMapColumns() const
    {
        return mapColumns_;
    }

    /*!
     * \brief Return the discretization width in x-direction
     */
    const Scalar getDeltaX() const
    {
        return deltaX_;
    }

    /*!
     * \brief Return the discretization width in z-direction
     */
    const Scalar getDeltaZ() const
    {
        return deltaZ_;
    }

    /*!
     * \brief Store a shared pointer to the fine-scale problem within the coarse-scale problem and vice versa
     */
    void setOtherProblem(std::shared_ptr<Problem> otherProblem)
    {
        otherProblem_ = otherProblem;
    }

    /*!
     * \brief Return the shared pointer of the fine-scale problem within the coarse-scale problem and vice versa
     */
    std::shared_ptr<Problem> getOtherProblem() const
    {
        return otherProblem_;
    }

    /*!
     * \brief Set the shared pointer to the fine-scale solution vector
     */
    void setFineSolution(std::shared_ptr<SolutionVector> fineSolution)
    {
        fineSolution_ = fineSolution;
    }

    /*!
     * \brief Return the shared pointer to the fine-scale solution vector
     */
    std::shared_ptr<SolutionVector> getFineSolution() const
    {
        return fineSolution_;
    }

    /*!
     * \brief Set the shared pointer to the coarse-scale solution vector
     */
    void setCoarseSolution(std::shared_ptr<SolutionVector> coarseSolution)
    {
        coarseSolution_ = coarseSolution;
    }

    /*!
     * \brief Return the shared pointer to the coarse-scale solution vector
     */
    SolutionVector getCoarseSolution() const
    {
        return *coarseSolution_;
    }

    /*!
     * \brief Return the coordinates of the lower left corner of the domain
     */
    const CellArray getUpperRightDomain() const
    {
        return upperRightDomain_;
    }

    /*!
     * \brief Return the coordinates of the lower left corner of the domain
     */
    const CellArray getLowerLeftDomain() const
    {
        return lowerLeftDomain_;
    }

    /*!
     * \brief Sets the time counter.
     */
    void setTimecounter(Scalar timecounter)
    { timecounter_ = timecounter; }

    /*!
     * \brief Gets the time counter.
     */
    const Scalar getTimecounter() const
    { return timecounter_; }

    /*!
     * \brief Sets the time loop pointer.
     */
    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    /*!
     * \brief Sets the time loop pointer.
     */
    const TimeLoopPtr getTimeLoop() const
    { return timeLoop_; }

    /*!
     * \brief Returns the time.
     */
    Scalar time() const
    { return timeLoop_->time(); }


    const std::shared_ptr<QuantityStorage> getQuantityStorage() const
    {
        return quantityStorage_;
    }


    //! Return a reference to the underlying spatial parameters
    const std::shared_ptr<SpatialParams> getSpatialParams() const
    { return spatialParams_; }

    //! Return a reference to the underlying spatial parameters
    const SpatialParams& spatialParams() const
    { return *spatialParams_; }

    //! Return a reference to the underlying spatial parameters
    SpatialParams& spatialParams()
    { return *spatialParams_; }


    std::vector<Scalar> returnDensityLiquidAndViscosity(const Scalar& temperature, const Scalar& pressure) const
    {
        return {WettingPhase::density(temperature,pressure), WettingPhase::viscosity(temperature,pressure)};
    }

    std::vector<Scalar> returnDensityGasAndViscosity(const Scalar& temperature, const Scalar& pressure) const
    {
        return {NonwettingPhase::density(temperature,pressure), NonwettingPhase::viscosity(temperature,pressure)};
    }

    /*!
     * \brief Return the dimension of the grid
     */
    Scalar getDim() const
    {
        return dim;
    };

private:

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }
    GlobalPosition DIMS_s;

    /*!
     * \brief Exports the multimap mapColumns_ to the spatialParams
     *
     * \param mapColumns mutltimap with column indices as keys and elements within respective column as values
     */
    void setSpatialParamsMapColumns_(std::shared_ptr<std::multimap<int, Element>> mapColumns)
    {
        this->spatialParams().setMapColumns(mapColumns);
    }

    /*!
     * \brief Calculates the coarse permeability in spatial params once and stores the values in a container
     */
    void calcPermeabilityAndPorosityCoarseInSpatialParams_()
    {
        this->spatialParams().calcSpatialParamsPermeabilityAndPorosityCoarse();
    }



    static constexpr Scalar eps_ = 1e-6;
    Scalar time_step;
    Scalar time_step_size;
    std::string problemName_;
    bool isCoarseVEModel_;
    mutable std::vector<double> H2_VEC;
    mutable std::vector<double> H2O_VEC;
    mutable std::vector<double> time_VEC;
    mutable std::vector<double> press_VEC;
    mutable std::vector<double> prodsat_VEC;
    mutable std::vector<double> sat_VEC;
    CellArray numberOfCells_;
    CellArray upperRightDomain_;
    CellArray lowerLeftDomain_;
    Scalar deltaX_;
    Scalar deltaZ_;
    std::shared_ptr<std::multimap<int, Element>> mapColumns_;
    std::shared_ptr<QuantityReconstructor> quantityReconstructor_;
    std::shared_ptr<QuantityStorage> quantityStorage_;
    
    std::shared_ptr<Problem> otherProblem_;

    std::shared_ptr<SolutionVector> fineSolution_, coarseSolution_;

    TimeLoopPtr timeLoop_;
    Scalar timecounter_;
    Scalar H2Orate_;
    Scalar H2rate_;
    
    //! Spatially varying parameters
    std::shared_ptr<SpatialParams> spatialParams_;
};

} // end namespace Dumux
#endif

