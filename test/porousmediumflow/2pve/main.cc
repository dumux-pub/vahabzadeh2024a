// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTests
 * \brief Test for the two-phase porousmedium flow model.
 */
#include <config.h>

#include <iostream>
#include <sstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/loadsolution.hh>

#include <dumux/verticalequilibriumcommon/quantityreconstruction.hh>
#include <dumux/verticalequilibriumcommon/util.hh>

#include "properties.hh"
#include "2pvesatreconst.hh"

#ifndef DIFFMETHOD
#define DIFFMETHOD DiffMethod::numeric
#endif

int main(int argc, char** argv)
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TwoPVEIncompressibleTpfa;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    //--------------------------------------------RESTRICTIONS CHECK--------------------------------------------
    VerticalEquilibriumModelUtil::checkForExceptionsOnlyVE();
    //----------------------------------------------------------------------------------------------------------

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init("");
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManagerCoarse;
    VerticalEquilibriumModelUtil::initializeCoarseGrid<TypeTag>(gridManagerCoarse); //from dumux/porousmediumflow/2pve/util.hh

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridViewFine = gridManager.grid().leafGridView();
    const auto& leafGridViewCoarse = gridManagerCoarse.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometryFine = std::make_shared<GridGeometry>(leafGridViewFine);
    auto gridGeometryCoarse = std::make_shared<GridGeometry>(leafGridViewCoarse);

    // create an object of the QuantityReconstructor that contains functions for quantity reconstruction to the fine scale
    using QuantityReconstructor = TwoPVEQuantityReconst<TypeTag>;
    QuantityReconstructor quantityReconstructorFine{};
    auto quantityReconstructorFineShared = std::make_shared<QuantityReconstructor>(quantityReconstructorFine);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problemCoarse = std::make_shared<Problem>(gridGeometryCoarse,gridGeometryFine, quantityReconstructorFineShared);
    auto problemFine = std::make_shared<Problem>(gridGeometryFine, problemCoarse);
    problemCoarse->setOtherProblem(problemFine);
    problemFine->setOtherProblem(problemCoarse);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    auto xCoarse = std::make_shared<SolutionVector>(gridGeometryCoarse->numDofs());
    auto xFine = std::make_shared<SolutionVector>(gridGeometryFine->numDofs());

    problemFine->applyInitialSolution(*xFine);
    problemCoarse->applyInitialSolution(*xCoarse);

    auto xOldFine = *xFine;
    auto xOldCoarse = *xCoarse;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariablesFine = std::make_shared<GridVariables>(problemFine, gridGeometryFine);
    gridVariablesFine->init(*xFine);

    auto gridVariablesCoarse = std::make_shared<GridVariables>(problemCoarse, gridGeometryCoarse);
    gridVariablesCoarse->init(*xCoarse);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd")*2629800;
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoopCoarse = std::make_shared<CheckPointTimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoopCoarse->setMaxTimeStepSize(maxDt);
    problemCoarse->setTimeLoop(timeLoopCoarse);
    problemFine->setTimeLoop(timeLoopCoarse);

    problemCoarse->setFineSolution(xFine);
    problemCoarse->setCoarseSolution(xCoarse);
    problemFine->setFineSolution(xFine);
    problemFine->setCoarseSolution(xCoarse);

    VtkOutputModule<GridVariables, SolutionVector> vtkWriterCoarse(*gridVariablesCoarse, *xCoarse, problemCoarse->name());
    using IOFieldsVECoarse = GetPropType<TypeTag, Properties::IOFields>;
    using VelocityOutputVE = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriterCoarse.addVelocityOutput(std::make_shared<VelocityOutputVE>(*gridVariablesCoarse));
    IOFieldsVECoarse::initOutputModule(vtkWriterCoarse, 1);
    vtkWriterCoarse.write(0.0);

    // initialize the darcyVEFine vtk output module
    VtkOutputModule<GridVariables, SolutionVector> vtkWriterReconstructedSolution(*gridVariablesFine, *xFine, problemFine->name());
    IOFieldsVECoarse::initOutputModule(vtkWriterReconstructedSolution, 0); // Add model specific output fields
    vtkWriterReconstructedSolution.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DIFFMETHOD>;
    auto assemblerCoarse = std::make_shared<Assembler>(problemCoarse, gridGeometryCoarse, gridVariablesCoarse, timeLoopCoarse, xOldCoarse);

    // the linear solver
    using LinearSolver = AMGBiCGSTABIstlSolver  <SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolverCoarse = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolverCoarse(assemblerCoarse, linearSolverCoarse);

    //--------------------------------------------HELPERS FOR PRINT MASS BALANCE--------------------------------------------
//     auto totalInjectedGasMass = 0.0;auto totalProducedGasMass = 0.0;auto totalProducedWaterMass = 0.0; auto remaining = 0.0;
    //=========================================FINISH HELPERS FOR PRINT MASS BALANCE=========================================

    Scalar qq=0;
    const Scalar outputTimeInterval = getParam<Scalar>("Problem.OutputTimeInterval");
    timeLoopCoarse->setPeriodicCheckPoint(outputTimeInterval);
    // time loop coarse
    timeLoopCoarse->start(); do
    {   
        if (timeLoopCoarse->time() < 1000)
            timeLoopCoarse->setTimeStepSize(5);

        double current_time = timeLoopCoarse->time();
        double timestepsize = timeLoopCoarse->timeStepSize();
        double newTimeStep = current_time + timestepsize;
        problemCoarse->setTimeStep(newTimeStep);
        problemCoarse->setTimeStepSize(timestepsize);
        // solve the non-linear system with time step control
        nonLinearSolverCoarse.solve(*xCoarse, *timeLoopCoarse);
        qq=qq+1;
        //--------------------------------------------HELPER FUNCTION FOR PRINT MASS BALANCE--------------------------------------------
        //helper function to print out wetting-phase and non-wetting-phase masses in the respective domains during the simulation
        // VerticalEquilibriumModelUtil::printMassBalanceVE<GridGeometry, SolutionVector, Problem, TimeLoop<Scalar>, Scalar>(totalInjectedGasMass,totalProducedGasMass,totalProducedWaterMass, remaining, gridGeometryCoarse, gridGeometryFine, xCoarse, problemCoarse, timeLoopCoarse);

        // make the new solution the old solution
        xOldCoarse = *xCoarse;
        gridVariablesCoarse->advanceTimeStep();
        if (timeLoopCoarse->isCheckPoint())
            problemCoarse->postTimeStep(xCoarse, gridGeometryCoarse, timeLoopCoarse);
        // advance to the time loop to the next step
        timeLoopCoarse->advanceTimeStep();
        if (timeLoopCoarse->isCheckPoint()){
            vtkWriterCoarse.write(timeLoopCoarse->time());
            vtkWriterReconstructedSolution.write(timeLoopCoarse->time());
        }
            

        // report statistics of this time step
        timeLoopCoarse->reportTimeStep();
        // set new dt as suggested by the Newton solver
        timeLoopCoarse->setTimeStepSize(nonLinearSolverCoarse.suggestTimeStepSize(timeLoopCoarse->timeStepSize()));
    } while (!timeLoopCoarse->finished());

    // output some Newton statistics
    nonLinearSolverCoarse.report();

    timeLoopCoarse->finalize(leafGridViewCoarse.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
