// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPVETests
 * \brief The spatial params for the incompressible 2p VE test.
 */

#ifndef DUMUX_INCOMPRESSIBLE_TWOPVE_TEST_SPATIAL_PARAMS_HH
#define DUMUX_INCOMPRESSIBLE_TWOPVe_TEST_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>
#include <dumux/porousmediumflow/2p/boxmaterialinterfaces.hh>
#include <dumux/common/random.hh>
#include <random>
namespace Dumux {

/*!
 * \ingroup TwoPVETests
 * \brief The spatial params for the incompressible 2p VE test.
 */
template<class GridGeometry, class Scalar>
class TwoPTestSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, TwoPTestSpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = TwoPTestSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, ThisType>;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;
    using PcKrSwCurve = FluidMatrix::BrooksCoreyDefault<Scalar>;
    

public:
    using PermeabilityType = Scalar;
    using Dimension_vector = Dune::FieldVector<Scalar, dimWorld+1>;;
    using Cell_vector = Dune::FieldVector<Scalar, dimWorld>;;
    TwoPTestSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , myPcKrSwCurve_("SpatialParams")
    , K_(0)
    {
        // intrinsic permeabilities
        permeability_ = getParam<PermeabilityType>("SpatialParams.Permeability")*9.869233e-16;
        DIMS=getParam<GlobalPosition>("Grid.Cells");
        LOC_X=getParam<GlobalPosition>("Grid.UpperRight");

        CELLS_VEC = getParam<GlobalPosition>("Grid.Cells");

        Perm_.resize(CELLS_VEC[1], CELLS_VEC[0]);

        DeltaX=LOC_X[0]/CELLS_VEC[0];
        DeltaZ=LOC_X[1]/CELLS_VEC[1];
        porosity_ = getParam<PermeabilityType>("SpatialParams.Porosity");
        PermH=getParam<PermeabilityType>("SpatialParams.Permeability")*9.869233e-16;
        permeability_=getParam<PermeabilityType>("SpatialParams.Permeability")*9.869233e-16;
        std::mt19937 rand(0);
    }

    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$
     *
     * \param globalPos The global position
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        return permeability_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        return porosity_;
    }


    /*!
     * \brief Returns the parameter object for the capillary-pressure/
     *        saturation material law
     *
     * \param globalPos The global position
     */
    const auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    {
        return makeFluidMatrixInteraction(myPcKrSwCurve_);
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        return FluidSystem::phase0Idx;
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
     *
     * This is not specific to the discretization. By default it just
     * throws an exception so it must be overloaded by the problem if
     * no energy equation is used.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 326.0; // 10°C
    }

private:
    const PcKrSwCurve myPcKrSwCurve_;

    Scalar permeability_;
    GlobalPosition DIMS;
    Dune::DynamicMatrix<Scalar> Perm_;
    Scalar porosity_;
    Scalar DeltaX;
    Scalar DeltaZ;
    Scalar PermH;
    Scalar DEV;
    DimWorldMatrix K_;
    static constexpr Scalar eps_ = 1.5e-7;
    GlobalPosition CELLS_VEC;
    Cell_vector CELLS_X;
    Cell_vector CELLS_Z;
    GlobalPosition LOC_X;
    Dimension_vector LOC_Z;
};

} // end namespace Dumux

#endif
