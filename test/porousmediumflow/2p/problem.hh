// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_INCOMPRESSIBLE_TWOP_TEST_PROBLEM_HH
#define DUMUX_INCOMPRESSIBLE_TWOP_TEST_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/fvproblem.hh>
#include <dumux/io/container.hh>
#include <dumux/porousmediumflow/problem.hh>

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

namespace Dumux {

/*!
 * \ingroup TwoPTests
 * \brief The incompressible 2p test problem.
 */
template<class TypeTag>
class TwoPTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using VolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::VolumeVariables;
    static constexpr int dimWorld = GridView::dimensionworld;
    enum {
        pressureH2OIdx = Indices::pressureIdx,
        saturationGasIdx = Indices::saturationIdx,
        contiGasEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };

public:
    using Dimension_vector = Dune::FieldVector<Scalar, dimWorld+1>;
    using Cell_vector = Dune::FieldVector<Scalar, dimWorld>;
    TwoPTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        problemName_ = getParam<std::string>("Problem.Name");
        LOC_X=getParam<GlobalPosition>("Grid.UpperRight");
        CELLS_VEC = getParam<GlobalPosition>("Grid.Cells");
        DeltaX=LOC_X[0]/CELLS_VEC[0];
        DeltaZ=LOC_X[1]/CELLS_VEC[1];
    }
    void setTimeStep(double newTimeStep) { time_step = newTimeStep; } // Public setter method for time_step
    void setTimeStepSize(double new_time_step_size) { time_step_size = new_time_step_size; } // Public setter method for time_step

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
        
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        GetPropType<TypeTag, Properties::FluidState> fluidState;
        fluidState.setTemperature(this->spatialParams().temperatureAtPos(globalPos));
        fluidState.setPressure(waterPhaseIdx, /*pressure=*/1.0e7);
        fluidState.setPressure(gasPhaseIdx, /*pressure=*/1.0e7);

        Scalar densityW = FluidSystem::density(fluidState, waterPhaseIdx);
        
        Scalar depth = this->gridGeometry().bBoxMax()[1] - globalPos[1];
        
        if(onRightBoundary_(globalPos))
        {
            if(getParam<bool>("Problem.EnableGravity"))
                values[pressureH2OIdx] = 1.0e7 - depth * densityW * this->spatialParams().gravity(globalPos)[1];
            else
                values[pressureH2OIdx] = 1.0e7;
            values[saturationGasIdx] = 0.0;
        }
        else
        {
            values[pressureH2OIdx] = 1.0e7;
            values[saturationGasIdx] = 1.0;
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    // #### Neumann boundaries
    // [[codeblock]]
    NumEqVector neumannAtPos(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        const auto& globalPos=scvf.ipGlobal();
        NumEqVector values(0.0);
        double X_min=0;
        double Y_min=0.0;
        X_min=this->gridGeometry().bBoxMin()[0];
        Y_min=this->gridGeometry().bBoxMin()[1];
        static const Scalar injectionDurationOp = getParam<double>("BoundaryConditions.InjectionDurationOp");
        static const Scalar extractionDurationOp = getParam<double>("BoundaryConditions.ExtractionDurationOp");
        static const Scalar InjectionDepth = getParam<double>("BoundaryConditions.InjectionDepth");
        const int cycleNumberOp = std::floor((time_step)/(injectionDurationOp+extractionDurationOp));
        const Scalar localTimeInCycle = time_step-cycleNumberOp*(injectionDurationOp+extractionDurationOp);
            if(globalPos[0]<X_min + eps_  && globalPos[1]-Y_min> InjectionDepth  && localTimeInCycle <= injectionDurationOp){
                values[contiGasEqIdx] = getParam<double>("BoundaryConditions.Injectionrate");
            }
            if (globalPos[0]<X_min + eps_ && globalPos[1]-Y_min> InjectionDepth && localTimeInCycle > injectionDurationOp){
                const Scalar elemMobW = elemVolVars[scvf.insideScvIdx()].mobility(waterPhaseIdx);      //Mobilities
                const Scalar elemMobN = elemVolVars[scvf.insideScvIdx()].mobility(gasPhaseIdx);
                Scalar fw=elemMobW/(elemMobW+elemMobN);
                Scalar rate = getParam<double>("BoundaryConditions.Productionrate"); // kg / (m * s)                              
                const Scalar qN=(1-fw)*rate; 
                const Scalar qW=fw*rate;
                values[contiGasEqIdx] = qN;
                values[contiWEqIdx] = qW;
            }
            
        
        return values;
    }

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        GetPropType<TypeTag, Properties::FluidState> fluidState;
        fluidState.setTemperature(this->spatialParams().temperatureAtPos(globalPos));
        fluidState.setPressure(waterPhaseIdx, /*pressure=*/1e7);
        fluidState.setPressure(gasPhaseIdx, /*pressure=*/1e7);

        Scalar densityW = FluidSystem::density(fluidState, waterPhaseIdx);

        Scalar depth = this->gridGeometry().bBoxMax()[1] - globalPos[1];

        // hydrostatic pressure
        values[pressureH2OIdx] = 1.0e7 - depth * densityW * this->spatialParams().gravity(globalPos)[1];
        values[saturationGasIdx] = 0.0;
        return values;
    }
    void postTimeStep(const SolutionVector& x,
                      std::shared_ptr<const GridGeometry> fvGridGeometry,
                      TimeLoopPtr timeLoop)
    {
        //define the volumeVariables. This container contains all primary and secondary variables
        VolumeVariables volVarsLocal;
        Scalar time=this->time_step;
        Scalar time_s=time;
        time=time/2629800;
        Scalar injection_rate=0.0;
        Scalar production_gas_rate=0.0;
        Scalar production_water_rate=0.0;
        Scalar AVG_SAT=0.0;
        Scalar Gas_sat_grid=0.0;
        Scalar sum_sat=0.0;
        Scalar phi_total=0.0;
        Scalar sum_sat_nw=0.0;
        Scalar sat_enum=0.0;
        double Delta_z=0.0; double Delta_x=0.0;
        double X_min=0;
        X_min=this->gridGeometry().bBoxMin()[0];
        Delta_z = DeltaZ;
        Delta_x = DeltaX;
        static const Scalar injectionDurationOp = getParam<double>("BoundaryConditions.InjectionDurationOp");
        static const Scalar extractionDurationOp = getParam<double>("BoundaryConditions.ExtractionDurationOp");
        static const Scalar InjectionDepth = getParam<double>("BoundaryConditions.InjectionDepth");
        static const Scalar Inj_Rate = getParam<double>("BoundaryConditions.Injectionrate");
        const int cycleNumberOp = std::floor((time_s)/(injectionDurationOp+extractionDurationOp));
        const Scalar localTimeInCycle = time_s-cycleNumberOp*(injectionDurationOp+extractionDurationOp);
        //loop over the elements of the grid
        for (const auto& element : Dune::elements(fvGridGeometry->gridView()))
        {
            //index of the element
            //local finite-volume geometry
            auto fvGeometryLocal = localView(*fvGridGeometry);
            fvGeometryLocal.bind(element);

            //solution belonging to the current element
            const auto elemSolLocal = elementSolution(element, x, *fvGridGeometry);

            //loop over the sub-control volumes. For the tpfa method, the elements and sub-control volumes are identical
            for (const auto& scv : scvs(fvGeometryLocal))
            {
                //update the volVars container to contain all quantitites for the current scv
                volVarsLocal.update(elemSolLocal, *this, element, scv);
                Gas_sat_grid=volVarsLocal.saturation(1);
                AVG_SAT=AVG_SAT+volVarsLocal.saturation(1);
                sum_sat=sum_sat+Gas_sat_grid;
                sat_enum=sat_enum+1;
                phi_total=phi_total+volVarsLocal.porosity();
                if(element.geometry().center()[0]-Delta_x/2<X_min+eps_ && element.geometry().center()[1]>InjectionDepth && localTimeInCycle <= injectionDurationOp)
                {   
                    const Scalar sat_nw = volVarsLocal.saturation(gasPhaseIdx);
                    sum_sat_nw=sum_sat_nw+sat_nw;  
                    injection_rate=injection_rate+Inj_Rate*Delta_z; // kg / (m * s)
                }
                if (element.geometry().center()[0]-Delta_x/2<X_min + eps_ && element.geometry().center()[1]> InjectionDepth && localTimeInCycle > injectionDurationOp){   
                    const Scalar sat_nw = volVarsLocal.saturation(gasPhaseIdx);
                    sum_sat_nw=sum_sat_nw+sat_nw;
                    sat_enum=sat_enum+1;
                    const Scalar elemMobW = volVarsLocal.mobility(waterPhaseIdx);      //Mobilities
                    const Scalar elemMobN = volVarsLocal.mobility(gasPhaseIdx);
                    Scalar fw=elemMobW/(elemMobW+elemMobN);
                    Scalar rate = getParam<double>("BoundaryConditions.Productionrate")*Delta_z; // kg / (m * s)
                    production_gas_rate=production_gas_rate+(1-fw)*rate;
                    production_water_rate=production_water_rate+fw*rate;
                }
            }
        }
        Scalar avg_prod_sat = sum_sat_nw/sat_enum;
        sat_VEC.push_back(AVG_SAT/(CELLS_VEC[0]*CELLS_VEC[1]));
        prodsat_VEC.push_back(avg_prod_sat);
        if (production_gas_rate!=0){
            H2_VEC.push_back(production_gas_rate);
        }
        if (injection_rate!=0){
            H2_VEC.push_back(injection_rate);
        }
        H2O_VEC.push_back(production_water_rate);
        time_VEC.push_back(time_s);
        writeContainerToFile(time_VEC, "time.txt");
        writeContainerToFile(H2O_VEC, "H2O.txt");
        writeContainerToFile(H2_VEC, "H2.txt");
        writeContainerToFile(prodsat_VEC, "Prodsat.txt");
        writeContainerToFile(sat_VEC, "sat.txt");
        time_old=time_s;
    }




private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }

    static constexpr Scalar eps_ = 1e-6;
    std::string problemName_;
    Scalar time_step;
    Scalar time_step_size;
    GlobalPosition DIMS_s;
    std::ofstream outputFile_;
    Scalar time_old;
    Scalar DeltaX;
    Scalar DeltaZ;
    GlobalPosition CELLS_VEC;
    Cell_vector CELLS_X;
    Cell_vector CELLS_Z;
    GlobalPosition LOC_X;
    Dimension_vector LOC_Z;
    std::vector<double> H2_VEC;
    std::vector<double> H2O_VEC;
    std::vector<double> time_VEC;
    std::vector<double> press_VEC;
    std::vector<double> sat_VEC;
    std::vector<double> prodsat_VEC;
};

} // end namespace Dumux

#endif


