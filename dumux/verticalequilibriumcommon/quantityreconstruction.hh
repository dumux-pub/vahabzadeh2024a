// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \ingroup TwoPVE
 * \brief Reconstructs quantities from the coarse scale to the fine scale for the incompressible 2p test.
 */
#ifndef DUMUX_TWOPVE_QUANTITYRECONSTRUCTION_HH
#define DUMUX_TWOPVE_QUANTITYRECONSTRUCTION_HH

#include <math.h>

#include <dumux/common/properties.hh>
#include <dumux/nonlinear/findscalarroot.hh>
#include "gasplumedistfunctions.hh"
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>
#include <tuple>

namespace Dumux {

// forward declarations
template<class TypeTag> class TwoPVEQuantityReconst;

/*!
 * \ingroup TwoPVE
 * \brief This class reconstructs the fine-scale quantities from the coarse-scale quantities.
 */
template<class TypeTag>
class TwoPVEQuantityReconst
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    enum {
        waterPhaseIdx = FluidSystem::phase0Idx, // = 0
        gasPhaseIdx = FluidSystem::phase1Idx, // = 1
        numPhases = FluidSystem::numPhases
    };
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using WettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::WettingPhase;
    using NonwettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::NonwettingPhase;


public:
    TwoPVEQuantityReconst(const std::string& spatialParamsGroup = "")
        : functionsHolder_()
    {}


    /*!
     * \brief Computes the gas plume distance gasPlumeDist
     *
     * \param storedMinGasPlumeDist the previously computed gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param residualSaturationPhases contains the phase residual saturation (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param domainHeight height of the whole domain
     * \param satWCoarse wetting-phase saturation (on the coarse scale)
     * \param BrooksCoreyParameters contains the two Brooks-Corey parameters (lambda and entry pressure)
     */
    const Scalar computeGasPlumeDist(const Scalar& storedMinGasPlumeDist,
                                     const std::tuple<Scalar,Scalar>& densityPhases,
                                     const std::tuple<Scalar,Scalar>& residualSaturationPhases,
                                     const Scalar& gravityNorm,
                                     const Scalar& domainHeight,
                                     const Scalar& satWCoarse,
                                     const std::tuple<Scalar,Scalar>& BrooksCoreyParameters,
                                     const Scalar absTol = 1e-11)
    {
        auto [densityW, densityNw] = densityPhases;
        auto [resSatW, resSatNw] = residualSaturationPhases;
        auto [BCLambda, BCEntryP] = BrooksCoreyParameters;

        Scalar gasPlumeDist{};
        Scalar xiStart = domainHeight/2.0;;
        functionsHolder_.update(BCLambda, BCEntryP, densityW, densityNw, domainHeight, gravityNorm, resSatW, resSatNw, satWCoarse);
        gasPlumeDist = findScalarRootNewton(xiStart, functionsHolder_.columnMassConservation(), 1e-11);

        return gasPlumeDist;
    }


    /*!
     * \brief Computes the capillary pressure on the coarse scale
     *
     * \param gasPlumeDist value of the gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param BCEntryP entry pressure of the Brook-Corey model
     */
    const Scalar computeCapillaryPressureCoarse(const Scalar& gasPlumeDist,
                                                const std::tuple<Scalar,Scalar>& densityPhases,
                                                const Scalar& gravityNorm,
                                                const Scalar& BCEntryP) const
    {
        auto [referenceDensityW, referenceDensityNw] = densityPhases;

        //calculate the coarse-scale capillary pressure
        const Scalar pcCoarse = gravityNorm * gasPlumeDist * (referenceDensityNw - referenceDensityW) + BCEntryP;
        return pcCoarse;
    }


    /*!
     * \brief Reconstructs the fine-scale wetting-phase and non-wetting phase pressures
     *
     * \param gasPlumeDist value of the gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param pressureWCoarse wetting-phase pressure on the coarse scale
     * \param BCEntryP entry pressure of the Brook-Corey model
     */
    const std::vector<Scalar> reconstPressure(const Scalar& gasPlumeDist,
                                              const std::tuple<Scalar,Scalar>& densityPhases,
                                              const Scalar& gravityNorm,
                                              const Scalar& height,
                                              const Scalar& pressureWCoarse,
                                              const Scalar& BCEntryP) const
    {
        auto [referenceDensityW, referenceDensityNw] = densityPhases;
        std::vector<Scalar> reconstructedPressures(numPhases, 0.0);

        //So far, only the capillaryFringe model is implemented
        if(height <= gasPlumeDist)
        {
            reconstructedPressures[waterPhaseIdx] = pressureWCoarse - referenceDensityW * gravityNorm * height;
            reconstructedPressures[gasPhaseIdx] = reconstructedPressures[waterPhaseIdx] + BCEntryP;
        }
        else
        {
            reconstructedPressures[waterPhaseIdx] = pressureWCoarse - referenceDensityW * gravityNorm * height;
            reconstructedPressures[gasPhaseIdx] = pressureWCoarse - referenceDensityW * gravityNorm * gasPlumeDist
                                                - referenceDensityNw * gravityNorm * (height - gasPlumeDist) + BCEntryP;
        }

        return reconstructedPressures;
    }


    /*!
     * \brief Reconstructs the fine-scale wetting-phase saturation
     * Compute the saturation as the mean of the reconstructed saturation over the height of a fine-scale cell.
     *
     * \param gasPlumeDistances contains the gas plume distance and the minimum gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param residualSaturationPhases contains the phase residual saturation (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param deltaZ discretizaion width of the fine-scale grid in vertical direction
     * \param BrooksCoreyParameters contains the two Brooks-Corey parameters (lambda and entry pressure)
     */
    const Scalar computeSaturationIntegral(const std::tuple<Scalar, Scalar>& gasPlumeDistances,
                                           const std::tuple<Scalar, Scalar>& densityPhases,
                                           const std::tuple<Scalar, Scalar>& residualSaturationPhases,
                                           const Scalar& gravityNorm,
                                           const Scalar& height,
                                           const Scalar& deltaZ,
                                           const std::tuple<Scalar, Scalar>&  BrooksCoreyParameters) const
    {
        auto [gasPlumeDist, minGasPlumeDist] = gasPlumeDistances;
        auto [referenceDensityW, referenceDensityNw] = densityPhases;
        auto [resSatW, resSatNw] = residualSaturationPhases;
        auto [BCLambda, BCEntryP] = BrooksCoreyParameters;

        const Scalar lowerBound = height - deltaZ/2.0;
        const Scalar upperBound = height + deltaZ/2.0;

        Scalar integral = 0.0;

/*      differentiate between three cases: 1) fine-scale element is completely under the gas plume,
                                           2) fine-scale element intersects with gas plume,
                                           3) fine-scale element is completely above the gas plume */
        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            integral += upperPartBound - lowerBound;
        }
        if(upperBound > minGasPlumeDist && lowerBound < gasPlumeDist )
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
            integral += (upperPartBound - lowerpartBound)*(1.0-resSatNw);
        }
        if(upperBound > gasPlumeDist)
        {
            const Scalar lowerPartBound = std::max(gasPlumeDist, lowerBound);

            integral += (std::pow(BCEntryP, BCLambda) * (1 - resSatW - resSatNw)) /
            ((1 - BCLambda) * (referenceDensityW - referenceDensityNw) * gravityNorm) *
            (std::pow(((upperBound - gasPlumeDist) * (referenceDensityW - referenceDensityNw) *
            gravityNorm + BCEntryP), (1 - BCLambda))
            - std::pow(((lowerPartBound - gasPlumeDist) * (referenceDensityW - referenceDensityNw) *
            gravityNorm + BCEntryP), (1 - BCLambda)))
            + resSatW * (upperBound - lowerPartBound);
        }

        integral = integral/(upperBound-lowerBound);
        return integral;
    }


    /*!
     * \brief Reconstructs the fine-scale capillary pressure
     *
     * \param gasPlumeDist value of the gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param BCEntryP entry pressure of the Brook-Corey model
     */
    const Scalar reconstCapillaryPressure(const Scalar& gasPlumeDist,
                                          const std::tuple<Scalar,Scalar>& densityPhases,
                                          const Scalar& gravityNorm,
                                          const Scalar& height,
                                          const Scalar& BCEntryP) const
    {
        auto [referenceDensityW, referenceDensityNw] = densityPhases;
        Scalar reconstCapillaryPressure = 0.0;

        //for capillary fringe model
        if(height <= gasPlumeDist)
        {
            reconstCapillaryPressure = BCEntryP;
        }
        else if(height > gasPlumeDist)
        {
            reconstCapillaryPressure = referenceDensityW * gravityNorm * (height - gasPlumeDist) + BCEntryP
                                     - referenceDensityNw * gravityNorm * (height - gasPlumeDist);
        }

        return reconstCapillaryPressure;
    }


    /*!
     * \brief Reconstructs the fine-scale wetting-phase and non wetting-phase mobilities
     *
     * In relation of the fine element's location to the gasPlumeDist, the reconstruction is conducted. The fine element is divided into even finer
     * segments in order to allow a better approximation of the integral.
     *
     * \param gasPlumeDistances contains the gas plume distance and the minimum gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param viscosityPhases contains the phase viscosities (here: 2 phases)
     * \param residualSaturationPhases contains the phase residual saturation (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param deltaZ discretizaion width of the fine-scale grid in vertical direction
     * \param subIntervalsY number of intervals for the sub-division of a fine-scale cell for computing the saturation integral
     * \param pcKrSwCurve relation for the capillary pressure - saturation (and relative permeability - saturation)
     * \param BrooksCoreyParameters contains the two Brooks-Corey parameters (lambda and entry pressure)
     */
    const std::vector<Scalar> reconstMobilitiesFine(const std::tuple<Scalar,Scalar>& gasPlumeDistances,
                                                    const std::tuple<Scalar,Scalar>& densityPhases,
                                                    const std::tuple<Scalar,Scalar>& viscosityPhases,
                                                    const std::tuple<Scalar,Scalar>& residualSaturationPhases,
                                                    const Scalar& gravityNorm,
                                                    const Scalar& height,
                                                    const Scalar& deltaZ,
                                                    const int& subIntervalsY,
                                                    FluidMatrix::BrooksCoreyDefault<Scalar> pcKrSwCurve,
                                                    const std::tuple<Scalar,Scalar>& BrooksCoreyParameters) const
    {
        auto [gasPlumeDist, minGasPlumeDist] = gasPlumeDistances;
        auto [viscosityW, viscosityNw] = viscosityPhases;

        std::vector<Scalar> mobilitesFine(numPhases);

        const Scalar lowerBound = height - deltaZ/2.0;
        const Scalar upperBound = height + deltaZ/2.0;

        Scalar mobilityWFine = 0.0;
        Scalar mobilityNwFine = 0.0;

        if(lowerBound < gasPlumeDist)
        {
            const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
            mobilityWFine += upperPartBound - lowerBound;
        }
        if(upperBound > gasPlumeDist)
        {
            const Scalar lowerPartBound = std::max(gasPlumeDist, lowerBound);
            const int intervalNumber = subIntervalsY * 2; //TODO: is this an ok assumption, twice the amount of vertical cells?
            const Scalar deltaZFiner = (upperBound - lowerPartBound)/intervalNumber;

            const std::vector<Scalar> saturationStart_newInterface = reconstSaturation_(gasPlumeDistances, densityPhases, residualSaturationPhases, gravityNorm, lowerPartBound, BrooksCoreyParameters);
            const std::vector<Scalar> saturationEnd_newInterface = reconstSaturation_(gasPlumeDistances, densityPhases, residualSaturationPhases, gravityNorm, upperBound, BrooksCoreyParameters);

            mobilityWFine += deltaZFiner / 2.0 * (pcKrSwCurve.krw(saturationStart_newInterface[0])
            + pcKrSwCurve.krw(saturationEnd_newInterface[0]));

            mobilityNwFine += deltaZFiner / 2.0 * (pcKrSwCurve.krn(saturationStart_newInterface[0])
            + pcKrSwCurve.krn(saturationEnd_newInterface[0]));

            for (int count = 1; count < intervalNumber; count++)
            {
                const std::vector<Scalar> saturation_newInterface = reconstSaturation_(gasPlumeDistances, densityPhases, residualSaturationPhases, gravityNorm, deltaZFiner * count + lowerPartBound, BrooksCoreyParameters);
                mobilityWFine += deltaZFiner * pcKrSwCurve.krw(saturation_newInterface[0]);
                mobilityNwFine += deltaZFiner * pcKrSwCurve.krn(saturation_newInterface[0]);
            }
        }

        mobilityWFine = mobilityWFine/(upperBound - lowerBound);
        mobilityNwFine = mobilityNwFine/(upperBound - lowerBound);

        mobilityWFine = mobilityWFine/viscosityW;
        mobilityNwFine = mobilityNwFine/viscosityNw;

        mobilitesFine[waterPhaseIdx] = mobilityWFine;
        mobilitesFine[gasPhaseIdx] = mobilityNwFine;

        return mobilitesFine;
    }

private:
    /*!
     * \brief Reconstructs the fine-scale wetting-phase and non wetting-phase saturation
     *
     * \param gasPlumeDistances contains the gas plume distance and the minimum gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param residualSaturationPhases contains the phase residual saturation (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param BrooksCoreyParameters contains the two Brooks-Corey parameters (lambda and entry pressure)
     */
    const std::vector<Scalar> reconstSaturation_(const std::tuple<Scalar,Scalar> gasPlumeDistances,
                                                 const std::tuple<Scalar,Scalar> densityPhases,
                                                 const std::tuple<Scalar,Scalar> residualSaturationPhases,
                                                 const Scalar& gravityNorm,
                                                 const Scalar& height,
                                                 const std::tuple<Scalar,Scalar> BrooksCoreyParameters) const
    {
        auto [gasPlumeDist, minGasPlumeDist] = gasPlumeDistances;
        auto [referenceDensityW, referenceDensityNw] = densityPhases;
        auto [resSatW, resSatNw] = residualSaturationPhases;
        auto [BCLambda, BCEntryP] = BrooksCoreyParameters;

        std::vector<Scalar> reconstructedSaturations(numPhases, 0.0);
        //for capillary fringe model
        reconstructedSaturations[waterPhaseIdx] = 1.0;
        reconstructedSaturations[gasPhaseIdx] = 0.0;

        if(height > minGasPlumeDist && height < gasPlumeDist)
        {
            reconstructedSaturations[waterPhaseIdx] = 1.0 - resSatNw;
            reconstructedSaturations[gasPhaseIdx] = resSatNw;
        }
        else if(height >= gasPlumeDist)
        {
            reconstructedSaturations[waterPhaseIdx] = std::pow(((height - gasPlumeDist) * (referenceDensityW - referenceDensityNw) * gravityNorm + BCEntryP),
                                                               (-BCLambda)) * std::pow(BCEntryP, BCLambda) * (1.0 - resSatW - resSatNw) + resSatW;
            reconstructedSaturations[gasPhaseIdx] = 1.0 - reconstructedSaturations[waterPhaseIdx];
        }
        return reconstructedSaturations;
    }

    TwoPVEGasPlumeFunctions functionsHolder_;
};

} // end namespace Dumux



#endif
