// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPVE
 * \brief Reconstructs quantities from the coarse scale to the fine scale for the incompressible 2p test.
 */
#ifndef DUMUX_TWOPVE_UTIL_HH
#define DUMUX_TWOPVE_UTIL_HH

#include <dumux/discretization/cellcentered/tpfa/fvelementgeometry.hh>

namespace Dumux {

namespace VerticalEquilibriumModelUtil {

    /*!
     * \brief Initializes the coarse grid
     *
     * Sets the number of vertical cells to 1 while keeping the coordinates of the top-right corner of the domain and the cells in horizontal direction
     *
     * \param gridManagerCoarse gridManager belonging to the coarse grid
     */
    template<class TypeTag>
    void initializeCoarseGrid(GridManager<GetPropType<TypeTag, Properties::Grid>>& gridManagerCoarse)
    {
        using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

        const GlobalPosition upperRightCoarse = getParam<GlobalPosition>("Grid.UpperRight");
        std::array<int, 2> cellsCoarse = getParam<std::array<int, 2>>("Grid.Cells");
        cellsCoarse[1] = 1;
        gridManagerCoarse.init(upperRightCoarse, cellsCoarse);
    }


    /*!
     * \brief Returns a multimap which maps the fine-grid elements to their respesctive coare-scale element indices
     *
     * Traverses all elements of the fine-scale grid and maps all elements belonging to one column of the domain to that column index
     *
     * \param gridGeometryFine gridGeometry of the fine-scale grid
     * \param gridGeometryCoarse gridGeometry of the coarse-scale grid
     */
    template<typename Element, typename GridGeometry, typename Scalar>
    const std::multimap<int, Element> createColumnMultiMapNew(std::shared_ptr<const GridGeometry> gridGeometryFine,
                                                              std::shared_ptr<const GridGeometry> gridGeometryCoarse)
    {
        std::multimap<int, Element> mapColumns;

        //generate multimap vor iterator over columns of fine grid
        for (const auto& element : Dune::elements(gridGeometryFine->gridView()))
        {
            for (const auto& elementCoarse : Dune::elements(gridGeometryCoarse->gridView()))
            {
                if(elementCoarse.geometry().center()[0] == element.geometry().center()[0]) //TODO: insert else: break or continue? Stop when appropriate column is found
                {
                    mapColumns.insert(std::make_pair(gridGeometryCoarse->elementMapper().index(elementCoarse), element));
                }
            }
        }

        return mapColumns;
    }


    /*
     * Define here which constraints need to be fulfilled for the lone VE model.
     */
    void checkForExceptionsOnlyVE()
    {
        //number of vertical cells for coarse VE domain has to be 1!-------------------------------------------------
        auto coarseCellsVE = getParam<std::vector<int>>("Grid.Cells");
        auto fineCellsVE = getParam<std::vector<int>>("Grid.FineCells");

        if(coarseCellsVE[0] != fineCellsVE[0])
        {
            throw std::invalid_argument("Number of horizontal cells for the coarse and fine VE model need to be the same!");
        }
    }


    /*!
     * \brief Print out the masses of the wetting phase and the non-wetting phase in the coarse VE and in the fine VE domain. For usage in test_2pve.
     *
     * \param totalInjectedGasMass reference to the variable that lives longer than this function for storing the cumulative injected gas over time
     * \param fvGridGeometryVE pointer to the gridGeometry of the coarse-scale grid
     * \param fvGridGeometryVEFine pointer to the gridGeometry of the fine-scale grid
     * \param solPointer pointer to the solution vector containing the solution for the coarse VE domain
     * \param darcyVerticalEquilibriumProblem pointer to the problem of the coarse-scale VE domain
     * \param timeLoop pointer to the timeLoop
     */
    template<typename GridGeometry, typename SolutionVector, typename ProblemVE, typename TimeLoop, typename Scalar>
    void printMassBalanceVE(double& totalInjectedGasMass, //change value per reference
                            Scalar& totalProducedGasMass, //change value per reference
                            Scalar& totalProducedWaterMass, //change value per reference
                            std::shared_ptr<const GridGeometry> fvGridGeometryVE,
                            std::shared_ptr<const GridGeometry> fvGridGeometryVEFine,
                            std::shared_ptr<SolutionVector> solPointer,
                            std::shared_ptr<ProblemVE> darcyVerticalEquilibriumProblem,
                            std::shared_ptr<TimeLoop> timeLoop)
    {
        auto injectionRate = getParam<Scalar>("BoundaryConditions.InjectionRate");

        //UpperRight of darcy and VE domain are supposed to be the same so far. Does not matter which domain's parameter we read in here
        Scalar domainHeight = getParam<std::vector<Scalar>>("Grid.UpperRight")[1]; //TODO: - lowerleft[1]
        Scalar domainWidth = getParam<std::vector<Scalar>>("Grid.UpperRight")[0]; //TODO: - lowerleft[0]


        //Using no volVars for VE domain
        std::cout << "\n ---------------------------------------info about mass conservation------------------------------------------------------------" << std::endl;
        //iterate over VE domain
        Scalar totalVolumeVE = 0.0;
        Scalar totalMassWVE = 0.0;
        Scalar totalMassNwVE = 0.0;

        //for fine-scale VE elements
        Scalar totalVolumeVEFine = 0.0;
        Scalar totalMassWVEFine = 0.0;
        Scalar totalMassNwVEFine = 0.0;

        Scalar myPorosity = getParam<Scalar>("SpatialParams.Porosity");

        //----------------------------------ITERATION OVER COARSE-SCALE ELEMENTS----------------------------------
        for (const auto& element : Dune::elements(fvGridGeometryVE->gridView()))
        {
            auto elementIdx = fvGridGeometryVE->elementMapper().index(element);

            auto satWCoarse = 1.0 - (*solPointer)[elementIdx][1];
            auto satNwCoarse = (*solPointer)[elementIdx][1];

            auto testDensityW = darcyVerticalEquilibriumProblem->getQuantityStorage()->getDensityWCoarse(elementIdx);
            auto testDensityNw = darcyVerticalEquilibriumProblem->getQuantityStorage()->getDensityNwCoarse(elementIdx);

            auto fvGeometryVE = localView(*fvGridGeometryVE);
            fvGeometryVE.bind(element);

            for (const auto& scvVE : scvs(fvGeometryVE))
            {
                totalVolumeVE += scvVE.volume();
                totalMassWVE += myPorosity * testDensityW * satWCoarse * scvVE.volume();
                totalMassNwVE += myPorosity * testDensityNw * satNwCoarse * scvVE.volume();
            }

            //----------------------------------ITERATION OVER FINE-SCALE ELEMENTS----------------------------------
            auto it = darcyVerticalEquilibriumProblem->getMapColumns()->lower_bound(elementIdx);

            for(; it != darcyVerticalEquilibriumProblem->getMapColumns()->upper_bound(elementIdx); it++)
            {
                auto elementIdxFine = fvGridGeometryVEFine->elementMapper().index(it->second);
                auto testDensityWFine = darcyVerticalEquilibriumProblem->getQuantityStorage()->getDensityWFine(elementIdxFine);
                auto testDensityNwFine = darcyVerticalEquilibriumProblem->getQuantityStorage()->getDensityNwFine(elementIdxFine);

                auto satWFine = darcyVerticalEquilibriumProblem->getQuantityStorage()->getSWFine(elementIdxFine);
                auto satNwFine = darcyVerticalEquilibriumProblem->getQuantityStorage()->getSNwFine(elementIdxFine);
                

                auto fvGeometryVEFine = localView(*fvGridGeometryVEFine);
                fvGeometryVEFine.bind(it->second);

                for (const auto& scv : scvs(fvGeometryVEFine))
                {
                    totalVolumeVEFine += scv.volume();
                    totalMassWVEFine += myPorosity * testDensityWFine * satWFine * scv.volume();
                    totalMassNwVEFine += myPorosity * testDensityNwFine * satNwFine * scv.volume();
                }
            }

            if(elementIdx==0)
            {
                darcyVerticalEquilibriumProblem->updateH2rate(element.geometry().center(), elementIdx);
                darcyVerticalEquilibriumProblem->updateH2Orate(element.geometry().center(), elementIdx);
            }
        }
        auto time_counter = darcyVerticalEquilibriumProblem->getTimeLoop();
        totalProducedGasMass += timeLoop->timeStepSize() * darcyVerticalEquilibriumProblem->getH2rate();
        totalProducedWaterMass += timeLoop->timeStepSize() * darcyVerticalEquilibriumProblem->getH2Orate();

        totalInjectedGasMass += timeLoop->timeStepSize() * domainHeight * (-injectionRate);

        std::cout << "Total volume is: " << totalVolumeVE << " while expected: " << domainHeight*domainWidth<< std::endl;
        std::cout << "Total injected gas mass is: " << totalInjectedGasMass << " while gas mass in VE coarse system is: " << totalMassNwVE<<  " and in VE fine system is: " << totalMassNwVEFine << std::endl;
        std::cout << "Total Produced gas mass is: " << totalProducedGasMass << " while gas mass in VE coarse system is: " << totalMassNwVE<<  " and in VE fine system is: " << totalMassNwVEFine << std::endl;
        std::cout << "Total Produced Eater mass is: " << totalProducedWaterMass << " while gas mass in VE coarse system is: " << totalMassNwVE<<  " and in VE fine system is: " << totalMassNwVEFine << std::endl;

        std::cout << "---------------------------------------------info end------------------------------------------------------------" << std::endl;
    }
} // end namespace VerticalEquilibriumModel

} // end namespace Dumux
#endif
