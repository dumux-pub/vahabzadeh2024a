// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPModel
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase model.
 */

#ifndef DUMUX_TWOPVE_VOLUMEVARIABLES_HH
#define DUMUX_TWOPVE_VOLUMEVARIABLES_HH

#include <dumux/porousmediumflow/volumevariables.hh>
#include <dumux/material/solidstates/updatesolidvolumefractions.hh>
#include <dumux/porousmediumflow/2pve/formulation.hh>

#include <tuple>

namespace Dumux {

/*!
 * \ingroup TwoPModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase model.
 */
template <class Traits>
class TwoPVEVolumeVariables
: public PorousMediumFlowVolumeVariables<Traits>
{
    using ParentType = PorousMediumFlowVolumeVariables<Traits>;
    using PermeabilityType = typename Traits::PermeabilityType;
    using ModelTraits = typename Traits::ModelTraits;
    using Idx = typename ModelTraits::Indices;
    using Scalar = typename Traits::PrimaryVariables::value_type;
    using FS = typename Traits::FluidSystem;
    static constexpr int numFluidComps = ParentType::numFluidComponents();
    enum
    {
        pressureIdx = Idx::pressureIdx,
        saturationIdx = Idx::saturationIdx,

        phase0Idx = FS::phase0Idx,
        phase1Idx = FS::phase1Idx,
        numPhases = FS::numPhases
    };

    static constexpr auto formulation = ModelTraits::priVarFormulation();
    using WettingPhase = typename Traits::WettingPhase;
    using NonwettingPhase = typename Traits::NonwettingPhase;

public:
    //! Export the type used for the primary variables
    using PrimaryVariables = typename Traits::PrimaryVariables;
    //! Export type of fluid system
    using FluidSystem = typename Traits::FluidSystem;
    //! Export type of fluid state
    using FluidState = typename Traits::FluidState;
    //! Export the indices
    using Indices = typename ModelTraits::Indices;
    //! Export type of solid state
    using SolidState = typename Traits::SolidState;
    //! Export type of solid system
    using SolidSystem = typename Traits::SolidSystem;

    /*!
     * \brief Updates all quantities for a given control volume.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problem The object specifying the problem which ought to
     *                be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub control volume
    */
    template<class ElemSol, class Problem, class Element, class Scv>
    void update(const ElemSol &elemSol,
                const Problem &problem,
                const Element &element,
                const Scv& scv) //TODO: now it is "hard-coded" for phase0 and phase1Idx instead of "wettingPhase" and "nonWettingPhase"
    {
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        std::vector<Scalar> mobilitiesCoarse(numPhases, 0.0);

        priVars_ = elemSol[scv.localDofIndex()];
        extrusionFactor_ = problem.spatialParams().extrusionFactor(element, scv, elemSol);

        //if problem is problemFine, elementIdx will return index of element in fine grid, else the index of the coarse grid
        const unsigned int elementIdx = problem.gridGeometry().elementMapper().index(element);
        const Scalar deltaZ = problem.getDeltaZ();

        const GlobalPosition elementPos = element.geometry().center();
        const Scalar temperature = problem.spatialParams().temperatureAtPos(elementPos);

        //Distinguish the coarse-scale (first if-branch) and fine-scale problem (second if-branch)
        if(problem.isCoarseVEModel() == true)
        {
            Scalar dim = problem.getDim(); //TODO: read in from gridView
            const Scalar domainHeight = problem.gridGeometry().bBoxMax()[dim-1];
            const Scalar gravityNorm = problem.spatialParams().gravity(elementPos).two_norm();
            const auto fluidMatrixInteraction = problem.spatialParams().fluidMatrixInteractionAtPos(elementPos);
            const Scalar resSatW = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
            const Scalar resSatNw = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
            const Scalar lambda = problem.spatialParams().getBrooksCoreyLambda();
            const Scalar entryP = problem.spatialParams().getBrooksCoreyEntryP();

            //----------coarse-scale volume variables----------

            //instead of using coarse element GlobalPosition, use its index -> faster computation
            const int columnIdx = problem.gridGeometry().elementMapper().index(element);
            const auto& priVars = elemSol[scv.localDofIndex()];

            const auto coarseSatW = 1.0 - priVars[1];
            const auto coarsePW = priVars[0];

            //use wetting-phase pressure for computation of all coarse-scale densities and viscosities
            fluidState_.setDensity(phase0Idx, WettingPhase::density(temperature, coarsePW));
            fluidState_.setViscosity(phase0Idx, WettingPhase::viscosity(temperature, coarsePW));
            fluidState_.setDensity(phase1Idx, NonwettingPhase::density(temperature, coarsePW));
            fluidState_.setViscosity(phase1Idx, NonwettingPhase::viscosity(temperature, coarsePW));

            //calculate gas plume distance of this coarse element
            Scalar storedMinGasPlumeDist = problem.getQuantityStorage()->getMinGasPlumeDist(elementIdx);
            Scalar gasPlumeDistThisColumn = problem.getQuantityReconstructor()->computeGasPlumeDist
                (storedMinGasPlumeDist,
                 std::make_tuple(this->density(phase0Idx),this->density(phase1Idx)),
                 std::make_tuple(resSatW,resSatNw),
                 gravityNorm,
                 domainHeight,
                 coarseSatW,
                 std::make_tuple(lambda,entryP));
            problem.getQuantityStorage()->storeGasPlumeDist(elementIdx, gasPlumeDistThisColumn);
            if(storedMinGasPlumeDist > gasPlumeDistThisColumn) //TODO: is this ok?
            {
                problem.getQuantityStorage()->storeMinGasPlumeDist(elementIdx, gasPlumeDistThisColumn);
            }

            //compute coarse-scale capillary pressure
            const Scalar pcCoarse = problem.getQuantityReconstructor()->computeCapillaryPressureCoarse
                (gasPlumeDistThisColumn,
                 std::make_tuple(this->density(phase0Idx), this->density(phase1Idx)),
                 gravityNorm,
                 entryP);

            //reconstruct fine-scale quantities
            std::vector<Scalar> reconstructedMobilites(numPhases, 0.0);
            GlobalPosition fineElementPos{};

            //retrieve the map which includes the pairing of coares elements and their respective fine-scale elements, however storing it would be too expensive, that is why it is only called
            typename std::map<int, Element>::const_iterator it = problem.getMapColumns()->lower_bound(columnIdx);
            auto upperBoundIterator = problem.getMapColumns()->upper_bound(columnIdx);

            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {
                fineElementPos = (it->second).geometry().center();
                const auto fineElementIdx = problem.getOtherProblem()->gridGeometry().elementMapper().index(it->second);

                //reconstruct fine-scale pressures
                std::vector<Scalar> reconstructedPressures = problem.getQuantityReconstructor()->reconstPressure
                    (gasPlumeDistThisColumn,
                    std::make_tuple(this->density(phase0Idx), this->density(phase1Idx)),
                    gravityNorm,
                    fineElementPos[1],
                    coarsePW,
                    entryP);

                //reconstruct fine-scale saturations
                Scalar storedMinGasPlumeDistCurrent = problem.getQuantityStorage()->getMinGasPlumeDist(elementIdx);
                Scalar reconstructedSaturationW = problem.getQuantityReconstructor()->computeSaturationIntegral
                    (std::make_tuple(gasPlumeDistThisColumn,storedMinGasPlumeDistCurrent),
                    std::make_tuple(this->density(phase0Idx),this->density(phase1Idx)),
                    std::make_tuple(resSatW,resSatNw),
                    gravityNorm,
                    fineElementPos[1],
                    deltaZ,
                    std::make_tuple(lambda,entryP));
                Scalar reconstructedSaturationNw = 1.0 - reconstructedSaturationW;

                //reconstruct fine-scale capillary pressures
                Scalar reconstructedCapillaryPressure = problem.getQuantityReconstructor()->reconstCapillaryPressure
                    (gasPlumeDistThisColumn,
                    std::make_tuple(this->density(phase0Idx),this->density(phase1Idx)),
                    gravityNorm,
                    fineElementPos[1],
                    entryP);

                //store fine-scale quantities
                problem.getQuantityStorage()->storePcFine(fineElementIdx, reconstructedCapillaryPressure);
                problem.getQuantityStorage()->storePWFine(fineElementIdx, reconstructedPressures[phase0Idx]);
                problem.getQuantityStorage()->storePNwFine(fineElementIdx, reconstructedPressures[phase1Idx]);
                problem.getQuantityStorage()->storeSWFine(fineElementIdx, reconstructedSaturationW);
                problem.getQuantityStorage()->storeSNwFine(fineElementIdx, reconstructedSaturationNw);


                //store coarse-scale densities as densities for columns on fine scale
                problem.getQuantityStorage()->storeDensityWFine(fineElementIdx, this->density(phase0Idx));
                problem.getQuantityStorage()->storeDensityNwFine(fineElementIdx, this->density(phase1Idx));
                problem.getQuantityStorage()->storeViscosityWFine(fineElementIdx, this->viscosity(phase0Idx));
                problem.getQuantityStorage()->storeViscosityNwFine(fineElementIdx, this->viscosity(phase1Idx));

                //calculate fine-scale mobilities
                reconstructedMobilites = problem.getQuantityReconstructor()->reconstMobilitiesFine
                    (std::make_tuple(gasPlumeDistThisColumn,storedMinGasPlumeDistCurrent),
                     std::make_tuple(this->density(phase0Idx),this->density(phase1Idx)),
                     std::make_tuple(this->viscosity(phase0Idx),this->viscosity(phase1Idx)),
                     std::make_tuple(resSatW,resSatNw),
                     gravityNorm,
                     fineElementPos[1],
                     deltaZ,
                     problem.getNumberOfCellsY(),
                     fluidMatrixInteraction.pcSwCurve(),
                     std::make_tuple(lambda,entryP));

                problem.getQuantityStorage()->storeMobilityWFine(fineElementIdx, reconstructedMobilites[phase0Idx]);
                problem.getQuantityStorage()->storeMobilityNwFine(fineElementIdx, reconstructedMobilites[phase1Idx]);

                //calculate coarse-scale mobilities as integrals of fine-scale mobilities
                mobilitiesCoarse[phase0Idx] += problem.spatialParams().permeabilityFineAtElement(it->second) * reconstructedMobilites[phase0Idx] * deltaZ;
                mobilitiesCoarse[phase1Idx] += problem.spatialParams().permeabilityFineAtElement(it->second) * reconstructedMobilites[phase1Idx] * deltaZ;
            }

            completeFluidStateCoarse(elemSol, problem, element, scv, fluidState_, solidState_, elementIdx, pcCoarse, gasPlumeDistThisColumn);

            const Scalar permeabilityCoarse = problem.spatialParams().permeability(element, scv, elemSol);

            mobilitiesCoarse[phase0Idx] /= permeabilityCoarse;
            mobilitiesCoarse[phase1Idx] /= permeabilityCoarse;

            mobility_[phase0Idx] = mobilitiesCoarse[phase0Idx];
            mobility_[phase1Idx] = mobilitiesCoarse[phase1Idx];

            //store for later usage in plotting
            problem.getQuantityStorage()->storeGasPlumeDist(elementIdx, gasPlumeDistThisColumn);
            problem.getQuantityStorage()->storeMobilityWCoarse(elementIdx, mobilitiesCoarse[phase0Idx]);
            problem.getQuantityStorage()->storeMobilityNwCoarse(elementIdx, mobilitiesCoarse[phase1Idx]);
            problem.getQuantityStorage()->storePWCoarse(elementIdx, this->pressure(phase0Idx));
            problem.getQuantityStorage()->storePNwCoarse(elementIdx, this->pressure(phase1Idx));
            problem.getQuantityStorage()->storePcCoarse(elementIdx, pcCoarse);
            problem.getQuantityStorage()->storeSaturationWCoarse(elementIdx, 1.0 - elemSol[scv.localDofIndex()][1]);
            problem.getQuantityStorage()->storeSaturationNwCoarse(elementIdx, elemSol[scv.localDofIndex()][1]);
            problem.getQuantityStorage()->storeDensityWCoarse(elementIdx, this->density(phase0Idx));
            problem.getQuantityStorage()->storeDensityNwCoarse(elementIdx, this->density(phase1Idx));
            problem.getQuantityStorage()->storeViscosityWCoarse(elementIdx, this->viscosity(phase0Idx));
            problem.getQuantityStorage()->storeViscosityNwCoarse(elementIdx, this->viscosity(phase1Idx));

            const auto& spatialParams = problem.spatialParams();
            // porosity calculation over inert volumefraction
            updateSolidVolumeFractions(elemSol, problem, element, scv, solidState_, numFluidComps);
            permeability_ = permeabilityCoarse;
            porosity_ = spatialParams.porosityCoarseAtElement(element); //porosity might already (also) be set in updateSolidVolumeFractions()
        }
        else //fine-scale
        {
            //----------fine-scale volume variables----------
            this->updateFine(problem, element); //this implementation is used for output. The function updateFine on its own is used in the couplingManager
        }
    }



    /*!
     * \brief Updates all quantities for a fine-scale element.
     *
     * \param problem The object specifying the problem which ought to
     *                be simulated (here: fine-scale problem)
     * \param element An element which contains part of the control volume
    */
    template<class Problem, class Element>
    void updateFine(const Problem &problem,
                    const Element &element)
    {
        //----------fine-scale volume variables----------

        //only needed for plotting
        const auto fineElementIdx = problem.gridGeometry().elementMapper().index(element);
        auto reconstructedSaturationW = problem.getQuantityStorage()->getSWFine(fineElementIdx);
        auto reconstructedSaturationNw = problem.getQuantityStorage()->getSNwFine(fineElementIdx);
        auto reconstructedCapillaryPressure = problem.getQuantityStorage()->getPcFine(fineElementIdx);
        auto reconstructedPressuresW = problem.getQuantityStorage()->getPWFine(fineElementIdx);
        auto reconstructedPressuresNw = problem.getQuantityStorage()->getPNwFine(fineElementIdx);

        const Scalar refTemperature = problem.getQuantityStorage()->referenceTemperature();
        completeFluidStateFine(problem, fluidState_, solidState_, fineElementIdx, refTemperature, reconstructedSaturationW, reconstructedSaturationNw, reconstructedCapillaryPressure, reconstructedPressuresW, reconstructedPressuresNw);

        mobility_[phase0Idx] = problem.getQuantityStorage()->getMobilityWFine(fineElementIdx);
        mobility_[phase1Idx] = problem.getQuantityStorage()->getMobilityNwFine(fineElementIdx);

        const auto& spatialParams = problem.spatialParams();
        permeability_ = spatialParams.permeabilityFineAtElement(element);
        porosity_ = spatialParams.porosityFineAtElement(element);
        extrusionFactor_ = spatialParams.extrusionFactorAtElement(element);
    }




/*!
     * \brief Sets complete fluid state. This function is used for the fine-scale elements.
     *
     * \param problemFine The object specifying the fine-scale problem which ought to be simulated
     * \param fluidState A container with the current (physical) state of the fluid
     * \param solidState A container with the current (physical) state of the solid
     * \param elementFineIdx Index of the fine-scale element
     * \param referenceTemperature value of the reference temperature
     * \param satW reconstructed wetting phase saturation for fine element with index: elementFineIdx
     * \param satNw reconstructed non-wetting phase saturation for fine element with index: elementFineIdx
     * \param pc reconstructed capillary pressure for fine element with index: elementFineIdx
     * \param pW reconstructed wetting phase pressure for fine element with index: elementFineIdx
     * \param pNw reconstructed non-wetting phase pressure for fine element with index: elementFineIdx
     *
     * Set saturations, capillary pressures, viscosities, densities and enthalpies.
     */
    template<class Problem>
    void completeFluidStateFine(Problem& problemFine,
                                FluidState& fluidState,
                                SolidState& solidState,
                                const int& elementFineIdx,
                                const Scalar& referenceTemperature,
                                const Scalar& satW,
                                const Scalar& satNw,
                                const Scalar& pc,
                                const Scalar& pW,
                                const Scalar& pNw)
    {
        fluidState.setTemperature(referenceTemperature);
        //This ONLY holds if phase0Idx is wetting phase and phase1Idx is non-wetting phase
        fluidState.setSaturation(phase1Idx, satNw);
        fluidState.setSaturation(phase0Idx, satW);

        fluidState.setPressure(phase0Idx, pW);
        pc_ = pc;
        fluidState.setPressure(phase1Idx, pNw);

        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState);

        for (int phaseIdx = 0; phaseIdx < ModelTraits::numFluidPhases(); ++phaseIdx)
        {
            // compute and set the viscosity
            Scalar mu = 0.0;
            if(phaseIdx == 0)
            {
                mu = problemFine.getQuantityStorage()->getViscosityWFine(elementFineIdx);
            }
            else if(phaseIdx == 1)
            {
                mu = problemFine.getQuantityStorage()->getViscosityNwFine(elementFineIdx);
            }
            fluidState.setViscosity(phaseIdx, mu);

            // compute and set the density
            Scalar rho = 0.0;
            if(phaseIdx == 0)
            {
                rho = problemFine.getQuantityStorage()->getDensityWFine(elementFineIdx);
            }
            else if(phaseIdx == 1)
            {
                rho = problemFine.getQuantityStorage()->getDensityNwFine(elementFineIdx);
            }
            fluidState.setDensity(phaseIdx, rho);
        }
    }



    /*!
     * \brief Sets complete fluid state. This function is used for the coarse-scale elements.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problemCoarse The object specifying the coarse-scale problem which ought to be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub-control volume
     * \param fluidState A container with the current (physical) state of the fluid
     * \param solidState A container with the current (physical) state of the solid
     * \param elementCoarseIdx Index of the coase-scale element
     * \param pcCoarse Coarsened capillary pressure of coarse-scale element
     * \param gasPlumeDist gas plume distance/height belonging to the coarse-scale element
     *
     * Set temperature, saturations, capillary pressures, viscosities, densities and enthalpies.
     */
    template<class ElemSol, class Problem, class Element, class Scv>
    void completeFluidStateCoarse(const ElemSol& elemSol,
                                  const Problem& problemCoarse,
                                  const Element& element,
                                  const Scv& scv,
                                  FluidState& fluidState,
                                  SolidState& solidState,
                                  const int& elementCoarseIdx,
                                  const Scalar& pcCoarse,
                                  const Scalar& gasPlumeDist)
    {
        const auto& spatialParams = problemCoarse.spatialParams();

        const auto& priVars = elemSol[scv.localDofIndex()];

        const auto wPhaseIdx = spatialParams.template wettingPhase<FluidSystem>(element, scv, elemSol);
        fluidState.setWettingPhase(wPhaseIdx);
        if (formulation == TwoPVEFormulation::p0s1)
        {
            fluidState.setPressure(phase0Idx, priVars[pressureIdx]);
            if (fluidState.wettingPhase() == phase1Idx)
            {
                fluidState.setSaturation(phase1Idx, priVars[saturationIdx]);
                fluidState.setSaturation(phase0Idx, 1 - priVars[saturationIdx]);

                pc_ = pcCoarse;
                fluidState.setPressure(phase1Idx, priVars[pressureIdx] + pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
            else
            {
                const auto Sn = Traits::SaturationReconstruction::reconstructSn(spatialParams, element,
                                                                                scv, elemSol, priVars[saturationIdx]);

                fluidState.setSaturation(phase1Idx, Sn);
                fluidState.setSaturation(phase0Idx, 1 - Sn);

                pc_ = pcCoarse; //this is our case currently
                fluidState.setPressure(phase1Idx, priVars[pressureIdx] + pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
        }
        else if (formulation == TwoPVEFormulation::p1s0)
        {
            fluidState.setPressure(phase1Idx, priVars[pressureIdx]);
            if (wPhaseIdx == phase1Idx)
            {
                const auto Sn = Traits::SaturationReconstruction::reconstructSn(spatialParams, element,
                                                                                scv, elemSol, priVars[saturationIdx]);
                fluidState.setSaturation(phase0Idx, Sn);
                fluidState.setSaturation(phase1Idx, 1 - Sn);

                pc_ = pcCoarse;
                fluidState.setPressure(phase0Idx, priVars[pressureIdx] - pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
            else
            {
                fluidState.setSaturation(phase0Idx, priVars[saturationIdx]);
                fluidState.setSaturation(phase1Idx, 1 - priVars[saturationIdx]);

                pc_ = pcCoarse;
                fluidState.setPressure(phase0Idx, priVars[pressureIdx] - pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
        }

        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState);
    }


    /*!
     * \brief Returns the phase state for the control volume.
     */
    const FluidState &fluidState() const
    { return fluidState_; }

    /*!
     * \brief Returns the phase state for the control volume.
     */
    const SolidState &solidState() const
    { return solidState_; }

    /*!
     * \brief Returns the saturation of a given phase within
     *        the control volume in \f$[-]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar saturation(int phaseIdx) const
    { return fluidState_.saturation(phaseIdx); }

    /*!
     * \brief Returns the mass density of a given phase within the
     *        control volume in \f$[kg/m^3]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar density(int phaseIdx) const
    { return fluidState_.density(phaseIdx); }

    /*!
     * \brief Returns the effective pressure of a given phase within
     *        the control volume in \f$[kg/(m*s^2)=N/m^2=Pa]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar pressure(int phaseIdx) const
    { return fluidState_.pressure(phaseIdx); }

    /*!
     * \brief Returns the capillary pressure within the control volume
     * in \f$[kg/(m*s^2)=N/m^2=Pa]\f$.
     */
    Scalar capillaryPressure() const
    { return pc_; }

    /*!
     * \brief Returns temperature inside the sub-control volume
     * in \f$[K]\f$.
     *
     * Note that we assume thermodynamic equilibrium, i.e. the
     * temperature of the rock matrix and of all fluid phases are
     * identical.
     */
    Scalar temperature() const
    { return fluidState_.temperature(/*phaseIdx=*/0); }

    /*!
     * \brief Returns the dynamic viscosity of the fluid within the
     *        control volume in \f$\mathrm{[Pa s]}\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar viscosity(int phaseIdx) const
    { return fluidState_.viscosity(phaseIdx); }

    /*!
     * \brief Returns the effective mobility of a given phase within
     *        the control volume in \f$[s*m/kg]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar mobility(int phaseIdx) const
    { return mobility_[phaseIdx]; }

    /*!
     * \brief Returns the average porosity within the control volume in \f$[-]\f$.
     */
    Scalar porosity() const
    { return porosity_; }

    /*!
     * \brief Returns the permeability within the control volume in \f$[m^2]\f$.
     */
    const PermeabilityType& permeability() const
    { return permeability_; }

    /*!
     * \brief Returns the wetting phase index
     */
    int wettingPhase() const
    {  return fluidState_.wettingPhase(); }


    /*!
     * \brief Returns the gas plume distance within a coarse-scale element
     */
    const Scalar& gasPlumedist() const
    { return gasPlumeDist_; }

    /*!
     * \brief Returns how much the sub-control volume is extruded.
     *
     * This means the factor by which a lower-dimensional (1D or 2D)
     * entity needs to be expanded to get a full dimensional cell. The
     * default is 1.0 which means that 1D problems are actually
     * thought as pipes with a cross section of 1 m^2 and 2D problems
     * are assumed to extend 1 m to the back.
     */
    Scalar extrusionFactor() const
    { return extrusionFactor_; }

protected:
    FluidState fluidState_;
    SolidState solidState_;

private:
    Scalar pc_;
    Scalar porosity_;
    PermeabilityType permeability_;
    Scalar mobility_[ModelTraits::numFluidPhases()];

    Scalar gasPlumeDist_;
    Scalar extrusionFactor_; //extrusionFactor from ParentType is "overloaded"

    PrimaryVariables priVars_;
};

} // end namespace Dumux

#endif
