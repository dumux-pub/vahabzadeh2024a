// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPVE
 * \brief Stores quantities for the fine scale and the coarse scale
 */
#ifndef DUMUX_TWOPVE_QUANTITYSTORAGE_HH
#define DUMUX_TWOPVE_QUANTITYSTORAGE_HH

#include <dumux/common/properties.hh>

namespace Dumux {

// forward declaration
template<class TypeTag> class TwoPVEQuantityStorage;

/*!
 * \ingroup TwoPVE
 * \brief This class stores quantities for the fine scale and the coarse scale
 */
template<class TypeTag>
class TwoPVEQuantityStorage
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    enum {
        waterPhaseIdx = FluidSystem::phase0Idx, // = 0
        gasPhaseIdx = FluidSystem::phase1Idx, // = 1
        numPhases = FluidSystem::numPhases
    };
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using CellArray = std::array<unsigned int, dimWorld>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;

public:

    TwoPVEQuantityStorage(CellArray& upperRightDomain,
                          std::shared_ptr<const GridGeometry> fvGridGeometryCoarse,
                          std::shared_ptr<const GridGeometry> fvGridGeometryFine)
        : gasPlumeDistElementCoarseVector_(fvGridGeometryCoarse->numScv(), upperRightDomain[1]),
          minGasPlumeDistElementCoarseVector_(fvGridGeometryCoarse->numScv(), upperRightDomain[1]),
          mobilityWCoarseVector_(fvGridGeometryCoarse->numScv()),
          mobilityNwCoarseVector_(fvGridGeometryCoarse->numScv()),
          saturationWCoarseVector_(fvGridGeometryCoarse->numScv()),
          saturationNwCoarseVector_(fvGridGeometryCoarse->numScv()),
          pWCoarseVector_(fvGridGeometryCoarse->numScv()),
          pNwCoarseVector_(fvGridGeometryCoarse->numScv()),
          pcCoarseVector_(fvGridGeometryCoarse->numScv()),
          pcFineVector_(fvGridGeometryFine->numScv()),
          pWFineVector_(fvGridGeometryFine->numScv()),
          pNwFineVector_(fvGridGeometryFine->numScv()),
          sWFineVector_(fvGridGeometryFine->numScv()),
          sNwFineVector_(fvGridGeometryFine->numScv()),
          mobilityWFineVector_(fvGridGeometryFine->numScv()),
          mobilityNwFineVector_(fvGridGeometryFine->numScv())
    {
        std::cout << "In quantityStorage. #fvGridGeometryCoarse scvs: " << fvGridGeometryCoarse->numScv() << std::endl;
        std::cout << "In quantityStorage. #fvGridGeometryFine scvs: " << fvGridGeometryFine->numScv() << std::endl;

        FluidState fluidStateReference;
        setFluidStateToReference(fluidStateReference);

        densityWCoarseVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.density(waterPhaseIdx));
        densityNwCoarseVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.density(gasPhaseIdx));
        viscosityWCoarseVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.viscosity(waterPhaseIdx));
        viscosityNwCoarseVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.viscosity(gasPhaseIdx));
        densityWFineVector_ = std::vector<Scalar>(fvGridGeometryFine->numScv(), fluidStateReference.density(waterPhaseIdx));
        densityNwFineVector_ = std::vector<Scalar>(fvGridGeometryFine->numScv(), fluidStateReference.density(gasPhaseIdx));
        viscosityWFineVector_ = std::vector<Scalar>(fvGridGeometryFine->numScv(), fluidStateReference.viscosity(waterPhaseIdx));
        viscosityNwFineVector_ = std::vector<Scalar>(fvGridGeometryFine->numScv(), fluidStateReference.viscosity(gasPhaseIdx));

        densityWCoarsePossibleVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.density(waterPhaseIdx));
        densityNwCoarsePossibleVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.density(gasPhaseIdx));
        viscosityWCoarsePossibleVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.viscosity(waterPhaseIdx));
        viscosityNwCoarsePossibleVector_ = std::vector<Scalar>(fvGridGeometryCoarse->numScv(), fluidStateReference.viscosity(gasPhaseIdx));

        std::cout << "In quantitystorage. pWCoarseVector_ has size: " << pWCoarseVector_.size() << " while pWFineVector_ has size: " << pWFineVector_.size() << std::endl;
    }


    const Scalar referencePressure() const
    {
        return 1.0e7;
    }

    const Scalar referenceTemperature() const
    {
        return 326.0;
    }

    void setFluidStateToReference(FluidState& fluidStateReference)
    {

        fluidStateReference.setPressure(waterPhaseIdx, referencePressure());
        fluidStateReference.setPressure(gasPhaseIdx, referencePressure());
        fluidStateReference.setTemperature(referenceTemperature());
        fluidStateReference.setSaturation(waterPhaseIdx, 1.);
        fluidStateReference.setSaturation(gasPhaseIdx, 0.);

        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidStateReference);

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx) {
            // compute and set the viscosity
            Scalar mu = FluidSystem::viscosity(fluidStateReference, paramCache, phaseIdx);
            fluidStateReference.setViscosity(phaseIdx, mu);

            // compute and set the density
            Scalar rho = FluidSystem::density(fluidStateReference, paramCache, phaseIdx);
            fluidStateReference.setDensity(phaseIdx, rho);
        }
    }


//_________________________________________GETTER&SETTER FUNCTIONS START HERE___________________________________

    /*!
     * \brief Store the gas plume distance of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param gasPlumeDist gas plume distance to be stored
     */
    void storeGasPlumeDist(const unsigned int& elementCoarseIdx, const Scalar& gasPlumeDist)
    {
        gasPlumeDistElementCoarseVector_[elementCoarseIdx] = gasPlumeDist;
    }

    /*!
     * \brief Return the stored gas plume distance of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getGasPlumeDist(const unsigned int elementCoarseIdx) const
    {
        return gasPlumeDistElementCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored gas plume distance vector
     */
    const std::vector<Scalar>& returnGasPlumeDistVector() const
    {
        return gasPlumeDistElementCoarseVector_;
    }



    /*!
     * \brief Store the minimal gas plume distance of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param minGasPlumeDist minimal gas plume distance to be stored
     */
    void storeMinGasPlumeDist(const unsigned int& elementCoarseIdx, const Scalar& minGasPlumeDist)
    {
        minGasPlumeDistElementCoarseVector_[elementCoarseIdx] = minGasPlumeDist;
    }

    /*!
     * \brief Return the stored minimal gas plume distance of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getMinGasPlumeDist(const unsigned int elementCoarseIdx) const
    {
        return minGasPlumeDistElementCoarseVector_[elementCoarseIdx];
    }



    /*!
     * \brief Store the wetting phase mobility of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param mobilityWCoarse wetting phase mobility to be stored
     */
    void storeMobilityWCoarse(const unsigned int& elementCoarseIdx, const Scalar& mobilityWCoarse)
    {
        mobilityWCoarseVector_[elementCoarseIdx] = mobilityWCoarse;
    }

    /*!
     * \brief Return the stored wetting phase mobility of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getMobilityWCoarse(const unsigned int elementCoarseIdx) const
    {
        return mobilityWCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored wetting phase mobility vector
     */
    const std::vector<Scalar>& returnMobilityWCoarseVector() const
    {
        return mobilityWCoarseVector_;
    }



    /*!
     * \brief Store the non-wetting phase mobility of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param mobilityNwCoarse non-wetting phase mobility to be stored
     */
    void storeMobilityNwCoarse(const unsigned int& elementCoarseIdx, const Scalar& mobilityNwCoarse)
    {
        mobilityNwCoarseVector_[elementCoarseIdx] = mobilityNwCoarse;
    }

    /*!
     * \brief Return the non-wetting phase mobility of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getMobilityNwCoarse(const unsigned int elementCoarseIdx) const
    {
        return mobilityNwCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase mobility vector
     */
    const std::vector<Scalar>& returnMobilityNwCoarseVector() const
    {
        return mobilityNwCoarseVector_;
    }


    /*!
     * \brief Store the wetting phase saturation of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param saturationWCoarse wetting phase saturation to be stored
     */
    void storeSaturationWCoarse(const unsigned int& elementCoarseIdx, const Scalar& saturationWCoarse)
    {
        saturationWCoarseVector_[elementCoarseIdx] = saturationWCoarse;
    }

    /*!
     * \brief Return the stored wetting phase saturation of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getSaturationWCoarse(const unsigned int elementCoarseIdx) const
    {
        return saturationWCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored wetting phase saturation vector
     */
    const std::vector<Scalar>& returnSaturationWCoarseVector() const
    {
        return saturationWCoarseVector_;
    }



    /*!
     * \brief Store the non-wetting phase saturation of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param saturationWCoarse wetting phase saturation to be stored
     */
    void storeSaturationNwCoarse(const unsigned int& elementCoarseIdx, const Scalar& saturationNwCoarse)
    {
        saturationNwCoarseVector_[elementCoarseIdx] = saturationNwCoarse;
    }

    /*!
     * \brief Return the stored wetting phase saturation vector
     */
    const std::vector<Scalar>& returnSaturationNwCoarseVector() const
    {
        return saturationNwCoarseVector_;
    }



     /*!
     * \brief Store the wetting phase pressure of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param pWCoarse wetting phase pressure to be stored
     */
    void storePWCoarse(const unsigned int& elementCoarseIdx, const Scalar& pWCoarse)
    {
        pWCoarseVector_[elementCoarseIdx] = pWCoarse;
    }

    const Scalar getPWCoarse(const unsigned int& elementCoarseIdx) const
    {
        return pWCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase pressure vector
     */
    const std::vector<Scalar>& returnPWCoarseVector() const
    {
        return pWCoarseVector_;
    }



    /*!
     * \brief Store the non-wetting phase pressure of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param pNwCoarse non-wetting phase pressure to be stored
     */
    void storePNwCoarse(const unsigned int& elementCoarseIdx, const Scalar& pNwCoarse)
    {
        pNwCoarseVector_[elementCoarseIdx] = pNwCoarse;
    }

    /*!
     * \brief Return the stored non-wetting phase pressure of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getPNwCoarse(const unsigned int elementCoarseIdx) const
    {
        return pNwCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase pressure vector
     */
    const std::vector<Scalar>& returnPNwCoarseVector() const
    {
        return pNwCoarseVector_;
    }



    /*!
     * \brief Store the capillary pressure of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param pcCoarse capillary pressure to be stored
     */
    void storePcCoarse(const unsigned int& elementCoarseIdx, const Scalar& pcCoarse)
    {
        pcCoarseVector_[elementCoarseIdx] = pcCoarse;
    }

    /*!
     * \brief Return the stored capillary pressure of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the fine-scale element
     */
    const Scalar getPcCoarse(const unsigned int elementCoarseIdx) const
    {
        return pcCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored capillary pressure vector
     */
    const std::vector<Scalar>& returnPcCoarseVector() const
    {
        return pcCoarseVector_;
    }



    /*!
     * \brief Store the capillary pressure of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param pcFine capillary pressure to be stored
     */
    void storePcFine(const unsigned int& elementFineIdx, const Scalar& pcFine)
    {
        pcFineVector_[elementFineIdx] = pcFine;
    }

    /*!
     * \brief Return the stored capillary pressure of a fine-scale element
     *
     * \param elementFineIdx Index of the coarse-scale element
     */
    const Scalar getPcFine(const unsigned int elementFineIdx) const
    {
        return pcFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored capillary pressure vector
     */
    const std::vector<Scalar>& returnPcFineVector() const
    {
        return pcFineVector_;
    }



    /*!
     * \brief Store the wetting phase pressure of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param pWFine wetting phase pressure to be stored
     */
    void storePWFine(const unsigned int& elementFineIdx, const Scalar& pWFine)
    {
        pWFineVector_[elementFineIdx] = pWFine;
    }

    /*!
     * \brief Return the stored wetting phase pressure of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getPWFine(const unsigned int elementFineIdx) const
    {
        return pWFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored wetting phase pressure vector
     */
    const std::vector<Scalar>& returnPWFineVector() const
    {
        return pWFineVector_;
    }



    /*!
     * \brief Store the non-wetting phase pressure of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param pNwFine non-wetting phase pressure to be stored
     */
    void storePNwFine(const unsigned int& elementFineIdx, const Scalar& pNwFine)
    {
        pNwFineVector_[elementFineIdx] = pNwFine;
    }

    /*!
     * \brief Return the stored non-wetting phase pressure of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getPNwFine(const unsigned int elementFineIdx) const
    {
        return pNwFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase pressure vector
     */
    const std::vector<Scalar>& returnPNwFineVector() const
    {
        return pNwFineVector_;
    }



    /*!
     * \brief Store the wetting phase saturation of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param sWFine wetting phase saturation to be stored
     */
    void storeSWFine(const unsigned int& elementFineIdx, const Scalar& sWFine)
    {
        sWFineVector_[elementFineIdx] = sWFine;
    }

    /*!
     * \brief Return the stored wetting phase saturation of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getSWFine(const unsigned int elementFineIdx) const
    {
        return sWFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored wetting phase saturation vector
     */
    const std::vector<Scalar>& returnSwFineVector() const
    {
        return sWFineVector_;
    }



    /*!
     * \brief Store the non-wetting phase saturation of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param sNwFine non-wetting phase saturation to be stored
     */
    void storeSNwFine(const unsigned int& elementFineIdx, const Scalar& sNwFine)
    {
        sNwFineVector_[elementFineIdx] = sNwFine;
    }

    /*!
     * \brief Return the stored non-wetting phase saturation of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getSNwFine(const unsigned int elementFineIdx) const
    {
        return sNwFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase saturation vector
     */
    const std::vector<Scalar>& returnSNwFineVector() const
    {
        return sNwFineVector_;
    }



    /*!
     * \brief Store the wetting phase mobility of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param mobilityWFine wetting phase mobility to be stored
     */
    void storeMobilityWFine(const unsigned int& elementFineIdx, const Scalar& mobilityWFine)
    {
        mobilityWFineVector_[elementFineIdx] = mobilityWFine;
    }

    /*!
     * \brief Return the stored wetting phase mobility of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getMobilityWFine(const unsigned int elementFineIdx) const
    {
        return mobilityWFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored wetting phase mobility vector
     */
    const std::vector<Scalar>& returnMobilityWFineVector() const
    {
        return mobilityWFineVector_;
    }



    /*!
     * \brief Store the non-wetting phase mobility of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param mobilityNwFine non-wetting phase mobility to be stored
     */
    void storeMobilityNwFine(const unsigned int& elementFineIdx, const Scalar& mobilityNwFine)
    {
        mobilityNwFineVector_[elementFineIdx] = mobilityNwFine;
    }

    /*!
     * \brief Return the stored non-wetting phase mobility of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getMobilityNwFine(const unsigned int elementFineIdx) const
    {
        return mobilityNwFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase mobility vector
     */
    const std::vector<Scalar>& returnMobilityNwFineVector() const
    {
        return mobilityNwFineVector_;
    }



    /*!
     * \brief Store the wetting phase density of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param densityWCoarse wetting phase density to be stored
     */
    void storeDensityWCoarse(const unsigned int elementCoarseIdx, const Scalar densityWCoarse)
    {
        densityWCoarseVector_[elementCoarseIdx] = densityWCoarse;
    }

    /*!
     * \brief Return the stored wetting phase density of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getDensityWCoarse(const unsigned int elementCoarseIdx) const
    {
        return densityWCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase mobility vector
     */
    const std::vector<Scalar>& returnDensityWCoarseVector() const
    {
        return densityWCoarseVector_;
    }



    /*!
     * \brief Store the non-wetting phase density of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param densityNwCoarse non-wetting phase density to be stored
     */
    void storeDensityNwCoarse(const unsigned int elementCoarseIdx, const Scalar densityNwCoarse)
    {
        densityNwCoarseVector_[elementCoarseIdx] = densityNwCoarse;
    }

    /*!
     * \brief Return the stored non-wetting phase density of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getDensityNwCoarse(const unsigned int elementCoarseIdx) const
    {
        return densityNwCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase mobility vector
     */
    const std::vector<Scalar>& returnDensityNwCoarseVector() const
    {
        return densityNwCoarseVector_;
    }



    /*!
     * \brief Store the wetting phase viscosity of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param viscosityWCoarse wetting phase viscosity to be stored
     */
    void storeViscosityWCoarse(const unsigned int elementCoarseIdx, const Scalar viscosityWCoarse)
    {
        viscosityWCoarseVector_[elementCoarseIdx] = viscosityWCoarse;
    }

    /*!
     * \brief Return the stored wetting phase viscosity of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getViscosityWCoarse(const unsigned int elementCoarseIdx) const
    {
        return viscosityWCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored wetting phase viscosity vector
     */
    const std::vector<Scalar>& returnViscosityWCoarseVector() const
    {
        return viscosityWCoarseVector_;
    }



    /*!
     * \brief Store the non-wetting phase viscosity of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param viscosityNwCoarse non-wetting phase viscosity to be stored
     */
    void storeViscosityNwCoarse(const unsigned int elementCoarseIdx, const Scalar viscosityNwCoarse)
    {
        viscosityNwCoarseVector_[elementCoarseIdx] = viscosityNwCoarse;
    }

    /*!
     * \brief Return the stored non-wetting phase viscosity of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getViscosityNwCoarse(const unsigned int elementCoarseIdx) const
    {
        return viscosityNwCoarseVector_[elementCoarseIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase viscosity vector
     */
    const std::vector<Scalar>& returnViscosityNwCoarseVector() const
    {
        return viscosityNwCoarseVector_;
    }


    /*!
     * \brief Store the wetting phase density of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param densityWFine wetting phase density to be stored
     */
    void storeDensityWFine(const unsigned int elementFineIdx, const Scalar densityWFine)
    {
        densityWFineVector_[elementFineIdx] = densityWFine;
    }

    /*!
     * \brief Return the stored wetting phase density of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getDensityWFine(const unsigned int elementFineIdx) const
    {
        return densityWFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored wetting phase density vector
     */
    const std::vector<Scalar>& returnDensityWFineVector() const
    {
        return densityWFineVector_;
    }



    /*!
     * \brief Store the non-wetting phase density of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param densityNwFine non-wetting phase density to be stored
     */
    void storeDensityNwFine(const unsigned int elementFineIdx, const Scalar densityNwFine)
    {
        densityNwFineVector_[elementFineIdx] = densityNwFine;
    }

    /*!
     * \brief Return the stored non-wetting phase density of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getDensityNwFine(const unsigned int elementFineIdx) const
    {
        return densityNwFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase density vector
     */
    const std::vector<Scalar>& returnDensityNwFineVector() const
    {
        return densityNwFineVector_;
    }



    /*!
     * \brief Store the wetting phase viscosity of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param viscosityWFine wetting phase viscosity to be stored
     */
    void storeViscosityWFine(const unsigned int elementFineIdx, const Scalar viscosityWFine)
    {
        viscosityWFineVector_[elementFineIdx] = viscosityWFine;
    }

    /*!
     * \brief Return the stored wetting phase viscosity of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getViscosityWFine(const unsigned int elementFineIdx) const
    {
        return viscosityWFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored wetting phase density vector
     */
    const std::vector<Scalar>& returnViscosityWFineVector() const
    {
        return viscosityWFineVector_;
    }



    /*!
     * \brief Store the non-wetting phase viscosity of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     * \param viscosityNwFine non-wetting phase viscosity to be stored
     */
    void storeViscosityNwFine(const unsigned int elementFineIdx, const Scalar viscosityNwFine)
    {
        viscosityNwFineVector_[elementFineIdx] = viscosityNwFine;
    }

    /*!
     * \brief Return the stored non-wetting phase viscosity of a fine-scale element
     *
     * \param elementFineIdx Index of the fine-scale element
     */
    const Scalar getViscosityNwFine(const unsigned int elementFineIdx) const
    {
        return viscosityNwFineVector_[elementFineIdx];
    }

    /*!
     * \brief Return the stored non-wetting phase density vector
     */
    const std::vector<Scalar>& returnViscosityNwFineVector() const
    {
        return viscosityNwFineVector_;
    }



    /*!
     * \brief Store the possible wetting phase density of a coarse-scale element. In the last iteration of the Newton-Solver
     * the relevant values are stored here and at the end of a time step shifted to densityWCoarseVector_
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param densityWCoarsePossible possible wetting phase density to be stored
     */
    void storeDensityWCoarsePossible(const unsigned int elementCoarseIdx, const Scalar densityWCoarsePossible)
    {
        densityWCoarsePossibleVector_[elementCoarseIdx] = densityWCoarsePossible;
    }

    /*!
     * \brief Return the stored possbile wetting phase density of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getDensityWCoarsePossible(const unsigned int elementCoarseIdx) const
    {
        return densityWCoarsePossibleVector_[elementCoarseIdx];
    }



    /*!
     * \brief Store the possible non-wetting phase density of a coarse-scale element. In the last iteration of the Newton-Solver
     * the relevant values are stored here and at the end of a time step shifted to densityNwCoarseVector_
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param densityNwCoarsePossible possible non-wetting phase density to be stored
     */
    void storeDensityNwCoarsePossible(const unsigned int elementCoarseIdx, const Scalar densityNwCoarsePossible)
    {
        densityNwCoarsePossibleVector_[elementCoarseIdx] = densityNwCoarsePossible;
    }

    /*!
     * \brief Return the stored possible non-wetting phase density of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getDensityNwCoarsePossible(const unsigned int elementCoarseIdx) const
    {
        return densityNwCoarsePossibleVector_[elementCoarseIdx];
    }



    /*!
     * \brief Store the possible wetting phase viscosity of a coarse-scale element. In the last iteration of the Newton-Solver
     * the relevant values are stored here and at the end of a time step shifted to viscosityWCoarseVector_
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param viscosityWCoarsePossible possible wetting phase viscosity to be stored
     */
    void storeViscosityWCoarsePossible(const unsigned int elementCoarseIdx, const Scalar viscosityWCoarsePossible)
    {
        viscosityWCoarsePossibleVector_[elementCoarseIdx] = viscosityWCoarsePossible;
    }

    /*!
     * \brief Return the stored possible wetting phase viscosity of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getViscosityWCoarsePossible(const unsigned int elementCoarseIdx) const
    {
        return viscosityWCoarsePossibleVector_[elementCoarseIdx];
    }



    /*!
     * \brief Store the possible non-wetting phase viscosity of a coarse-scale element. In the last iteration of the Newton-Solver
     * the relevant values are stored here and at the end of a time step shifted to viscosityNwCoarseVector_
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param viscosityNwCoarsePossible possible non-wetting phase viscosity to be stored
     */
    void storeViscosityNwCoarsePossible(const unsigned int elementCoarseIdx, const Scalar viscosityNwCoarsePossible)
    {
        viscosityNwCoarsePossibleVector_[elementCoarseIdx] = viscosityNwCoarsePossible;
    }

    /*!
     * \brief Return the stored possible non-wetting phase viscosity of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getViscosityNwCoarsePossible(const unsigned int elementCoarseIdx) const
    {
        return viscosityNwCoarsePossibleVector_[elementCoarseIdx];
    }



    /*!
     * \brief This is called at the end of a time-step in order to set the densities and viscosites of all coarse-scale elements
     *        to the densities and viscosities calculated in the last Newton-Iteration before converging
     */
    void setPossibleDensitiesAndViscositiesToFinal()
    {
        for(int i=0; i<densityWCoarsePossibleVector_.size(); i++)
        {
            densityWCoarseVector_[i] = densityWCoarsePossibleVector_[i];
            densityNwCoarseVector_[i] = densityNwCoarsePossibleVector_[i];
            viscosityWCoarseVector_[i] = viscosityWCoarsePossibleVector_[i];
            viscosityNwCoarseVector_[i] = viscosityNwCoarsePossibleVector_[i];
        }
    }

private:
    std::vector<Scalar> gasPlumeDistElementCoarseVector_,
                        minGasPlumeDistElementCoarseVector_,
                        mobilityWCoarseVector_,
                        mobilityNwCoarseVector_,
                        saturationWCoarseVector_,
                        saturationNwCoarseVector_,
                        pWCoarseVector_,
                        pNwCoarseVector_,
                        pcCoarseVector_,
                        pcFineVector_,
                        pWFineVector_,
                        pNwFineVector_,
                        sWFineVector_,
                        sNwFineVector_,
                        mobilityWFineVector_,
                        mobilityNwFineVector_,
                        densityWCoarseVector_,
                        densityNwCoarseVector_,
                        viscosityWCoarseVector_,
                        viscosityNwCoarseVector_,
                        densityWFineVector_,
                        densityNwFineVector_,
                        viscosityWFineVector_,
                        viscosityNwFineVector_,
                        densityWCoarsePossibleVector_,
                        densityNwCoarsePossibleVector_,
                        viscosityWCoarsePossibleVector_,
                        viscosityNwCoarsePossibleVector_;
};
} // end namespace Dumux

#endif
